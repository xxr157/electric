package cn.itcast.elec.web.action;


import java.util.List;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.service.IElecCommonMsgService;
import cn.itcast.elec.web.form.ElecCommonMsgForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * @author Administrator
 *
 */
public class ElecCommonMsgAction extends BaseAction implements ModelDriven<ElecCommonMsgForm>{
	private ElecCommonMsgForm elecCommonMsgForm=new ElecCommonMsgForm();
	//写在父类中了
	//private HttpServletRequest request=null;
	//控制层调用业务层
	private IElecCommonMsgService elecCommonMsgService=(IElecCommonMsgService)ServiceProvider.getService(IElecCommonMsgService.SERVICE_NAME);

	@Override
	public ElecCommonMsgForm getModel() {
		return elecCommonMsgForm;
	}
	/*
	 * 查询所有待办事宜列表
	 */
	public String home(){
		List<ElecCommonMsgForm> list = elecCommonMsgService.findElecCommonMsgList();
		request.setAttribute("commonList", list);
		return "home";
	}
	/**
	 * 保存待办事宜
	 */
	public String save(){
		elecCommonMsgService.saveElecCommonMsg(elecCommonMsgForm);
		return "save";
	}

}
