package cn.itcast.elec.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.itcast.elec.web.form.ElecLogForm;

public interface IElecLogService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecLogServiceImpl";

	public void saveElecLog(HttpServletRequest request, String details);

	public List<ElecLogForm> findElecLogListByCondition(ElecLogForm elecLogForm);

	public void deleteElecLogByLogIds(ElecLogForm elecLogForm);
	
}
