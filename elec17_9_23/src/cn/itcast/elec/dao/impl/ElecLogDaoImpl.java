package cn.itcast.elec.dao.impl;

import org.springframework.stereotype.Repository;

import cn.itcast.elec.common.CommonDaoImpl;
import cn.itcast.elec.dao.IElecLogDao;
import cn.itcast.elec.domain.ElecLog;
//用spring注入dao实现对象
@Repository(IElecLogDao.SERVICE_NAME)
public class ElecLogDaoImpl extends CommonDaoImpl<ElecLog> implements IElecLogDao {
	
}
