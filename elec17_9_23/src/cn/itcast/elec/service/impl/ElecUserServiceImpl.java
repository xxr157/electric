package cn.itcast.elec.service.impl;


import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecSystemDDLDao;
import cn.itcast.elec.dao.IElecUserDao;
import cn.itcast.elec.domain.ElecUser;
import cn.itcast.elec.service.IElecUserService;
import cn.itcast.elec.util.GenerateSqlFromExcel;
import cn.itcast.elec.util.MD5keyBean;
import cn.itcast.elec.util.PageInfo;
import cn.itcast.elec.util.StringHelper;
import cn.itcast.elec.web.form.ElecUserForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecUserService.SERVICE_NAME)
public class ElecUserServiceImpl implements IElecUserService{
	@Resource(name=IElecUserDao.SERVICE_NAME)
	private IElecUserDao elecUserDao;
	// 引入数据字典
	@Resource(name=IElecSystemDDLDao.SERVICE_NAME)
	private IElecSystemDDLDao elecSystemDDLDao;
	/**
	 * @description 查询所有用户列表信息
	 * 				判断用户姓名是否为空，如果不为空，按照用户姓名组织查询条件
	 * @create date 2017-12-29
	 * @param ElecUserForm elecUserForm VO对象存放用户姓名
	 * @return List<ElecUserForm>用户信息结果集对象
	 */
	@Override
	public List<ElecUserForm> findElecUserList(ElecUserForm elecUserForm,HttpServletRequest request) {
		// 组织查询条件
		String hqlWhere = "";
		List<String> paramsList = new ArrayList<String>();
		if(elecUserForm!=null && elecUserForm.getUserName()!=null && !elecUserForm.getUserName().equals("")){
			hqlWhere += " and o.userName like ?";
			paramsList.add("%" + elecUserForm.getUserName() + "%");
		}
		// 集合转换成数组
		Object[] params = paramsList.toArray();
		// 组织排序
		LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
		// 按入职时间降序排序
		orderby.put("o.onDutyDate", "desc");
		// begin:2018-02-27修改，添加分页功能 
		PageInfo pageInfo = new PageInfo(request);
		// 执行底层查询条，返回PO对象
//		List<ElecUser> list = elecUserDao.findCollectionByConditionNoPage(hqlWhere, params, orderby);
		List<ElecUser> list = elecUserDao.findCollectionByConditionWithPage(hqlWhere, params, orderby ,pageInfo);
		request.setAttribute("page", pageInfo.getPageBean());
		// end
		// 将PO对象转换为VO对象
		List<ElecUserForm> formList = this.elecUserPOListToVOList(list);
		return formList;
	}
	// 将PO对象转换为VO对象
	private List<ElecUserForm> elecUserPOListToVOList(List<ElecUser> list) {
		List<ElecUserForm> formList = new ArrayList<ElecUserForm>();
		ElecUserForm elecUserForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			ElecUser elecUser =list.get(i);
			elecUserForm = new ElecUserForm();
			// 将Integer类型转换为String类型
			elecUserForm.setUserID(elecUser.getUserID()); //主键id
			elecUserForm.setLogonName(elecUser.getLogonName()); // 登录名
			elecUserForm.setUserName(elecUser.getUserName()); // 用户姓名
			// 将code转换为数据字典中的value值显示到页面
			// 通过性别code和对应数据类型查询性别value值
			elecUserForm.setSexID(elecUser.getSexID()!=null && !elecUser.getSexID().equals("")?elecSystemDDLDao.findDDLName(elecUser.getSexID(),"性别"):""); // 性别
			elecUserForm.setContactTel(elecUser.getContactTel()); // 联系电话
			// 将code转换为数据字典中的value值显示到页面
			// 通过是否在职code和对应数据类型查询是否在职value值
			elecUserForm.setIsDuty(elecUser.getIsDuty()!=null && !elecUser.getIsDuty().equals("")?elecSystemDDLDao.findDDLName(elecUser.getIsDuty(),"是否在职"):""); // 是否在职
			formList.add(elecUserForm);
		}
		return formList;
		
	}
	/**
	 * @description 保存用户添加页面信息,需将此方法的事务定义为可写的
	 * @create date 2017-12-31
	 * @param ElecUserForm elecUserForm VO对象用于存放用户信息
	 * @return 无
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	@Override
	public void saveElecUser(ElecUserForm elecUserForm) {
		// 将VO对象转换成PO对象
		ElecUser elecUser = this.elecUserVOToPO(elecUserForm);
		// 从页面获取userID的值，如果userID不为空则执行更新操作
		if(elecUserForm.getUserID()!=null && !elecUserForm.getUserID().equals("")){
			elecUserDao.update(elecUser);
		}else{// 如果userID为空则执行保存操作
			elecUserDao.save(elecUser);
		}
	}
	/*
	 * 将页面用户信息从VO对象转换成PO对象
	 */
	private ElecUser elecUserVOToPO(ElecUserForm elecUserForm) {
		ElecUser elecUser = new ElecUser();
		if(elecUserForm!=null){
			if(elecUserForm.getUserID()!=null && !elecUserForm.getUserID().equals("")){
				// 出现的严重问题：将elecUser.setUserID写成了elecUser.setJctID，导致update不起作用，但save方法起作用
				elecUser.setUserID(elecUserForm.getUserID());
				if(elecUserForm.getOffDutyDate()!=null && !elecUserForm.getOffDutyDate().equals("")){
					elecUser.setOffDutyDate(StringHelper.stringCoverDate(elecUserForm.getOffDutyDate()));
				}
			}
			elecUser.setJctID(elecUserForm.getJctID());
			elecUser.setUserName(elecUserForm.getUserName());
			elecUser.setLogonName(elecUserForm.getLogonName());
			/**************** begin******************/
			// 2018-01-29修改对密码进行MD5加密
			String password = elecUserForm.getLogonPwd();
			String md5Password = "";
			// 如果没有设置密码，则默认密码为6个0
			if(password==null || "".equals(password)){
				password = "000000";
			}
			String md5flag = elecUserForm.getMd5flag();
			// 修改用户时,若前后两次密码一致,则不需要进行加密
			if(md5flag!=null && md5flag.equals("1")){
				md5Password=password;
			}
			// 无论是新增用户还是修改密码,都需要加密
			else{
				MD5keyBean md5 = new MD5keyBean();
				md5Password = md5.getkeyBeanofStr(password);
			}
			elecUser.setLogonPwd(md5Password);
			/***********************end**************************/
			elecUser.setSexID(elecUserForm.getSexID());
			// 从字符串类型转换为日期类型
			if(elecUserForm.getBirthday()!=null && !elecUserForm.getBirthday().equals("")){
				elecUser.setBirthday(StringHelper.stringCoverDate(elecUserForm.getBirthday()));
			}
			elecUser.setAddress(elecUserForm.getAddress());
			elecUser.setContactTel(elecUserForm.getContactTel());
			elecUser.setEmail(elecUserForm.getEmail());
			elecUser.setMobile(elecUserForm.getMobile());
			elecUser.setIsDuty(elecUserForm.getIsDuty());
			// 从字符串类型转换为日期类型
			if(elecUserForm.getOnDutyDate()!=null && !elecUserForm.getOnDutyDate().equals("")){
				elecUser.setOnDutyDate(StringHelper.stringCoverDate(elecUserForm.getOnDutyDate()));
			}
			elecUser.setRemark(elecUserForm.getRemark());
		}
		return elecUser;
	}
	/**
	 * @description 使用用户ID进行查询，获取用户的详细信息
	 * @create date 2017-12-31
	 * @param ElecUserForm elecUserForm VO对象用于存放用户ID
	 * @return ElecUserForm VO对象存放用户的详细信息
	 */
	@Override
	public ElecUserForm findElecUser(ElecUserForm elecUserForm) {
		String userID = elecUserForm.getUserID();
		ElecUser elecUser = elecUserDao.findObjectById(userID);
		// PO对象转换成VO对象,方式一：这种方法需要在ElecUserAction使用值栈的方式传递elecUserForm对象
//		elecUserForm = this.elecUserPOToVO(elecUser);
		// PO对象转换成VO对象,方式二：使用参数的方式传递elecUserForm对象，不需要在ElecUserAction中使用值栈的方式传递
		elecUserForm = this.elecUserPOToVO(elecUser,elecUserForm);
		return elecUserForm;
	}
	// PO对象转换成VO对象
	private ElecUserForm elecUserPOToVO(ElecUser elecUser,ElecUserForm elecUserForm) {
//		ElecUserForm elecUserForm = new ElecUserForm();
		if(elecUser!=null){
			elecUserForm.setUserID(elecUser.getUserID());
			elecUserForm.setJctID(elecUser.getJctID());
			elecUserForm.setUserName(elecUser.getUserName());
			elecUserForm.setLogonName(elecUser.getLogonName());
			elecUserForm.setLogonPwd(elecUser.getLogonPwd());
			elecUserForm.setSexID(elecUser.getSexID());
			elecUserForm.setBirthday(String.valueOf(elecUser.getBirthday()!=null && !elecUser.getBirthday().equals("")?elecUser.getBirthday():""));
			elecUserForm.setAddress(elecUser.getAddress());
			elecUserForm.setContactTel(elecUser.getContactTel());
			elecUserForm.setEmail(elecUser.getEmail());
			elecUserForm.setMobile(elecUser.getMobile());
			elecUserForm.setIsDuty(elecUser.getIsDuty());
			elecUserForm.setOnDutyDate(String.valueOf(elecUser.getOnDutyDate()!=null && !elecUser.getOnDutyDate().equals("")?elecUser.getOnDutyDate():""));
			elecUserForm.setOffDutyDate(String.valueOf(elecUser.getOffDutyDate()!=null && !elecUser.getOffDutyDate().equals("")?elecUser.getOffDutyDate():""));
			elecUserForm.setRemark(elecUser.getRemark());
		}
		return elecUserForm;
	}
	/**
	 * @description 从页面获取userID的值，通过userID的值删除用户信息，将事务设置为可写类型
	 * @create date 2017-1-4
	 * @param ElecUserForm elecUserForm VO对象用于存放用户ID
	 * @return 无
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	@Override
	public void deleteElecUser(ElecUserForm elecUserForm) {
		// 获取页面传递的userID值
		String userID = elecUserForm.getUserID();
		// 根据userID删除所选对象
		elecUserDao.deleteObjectByIDs(userID);
	}
	/**
	 * @description 查询当前数据库中是否已存在要添加的登录名
	 * @create date 2017-1-4
	 * @param String logonName 编辑页面获取的用户登录名
	 * @return String  checkflag=1：如果值为1，说明当前登录名在数据库中有重复记录，则不能进行保存
	 *                 checkflag=2：如果值为2，说明当前登录名在数据库中没有重复值，可以进行保存
	 */
	@Override
	public String checkLogonName(String logonName) {
		String hqlWhere = " and o.logonName = ?";
		Object [] params = {logonName};
		List<ElecUser> list = elecUserDao.findCollectionByConditionNopage(hqlWhere, params, null);
		String checkflag = "";
		if(list!=null && list.size()>0){
			checkflag = "1";
		}else{
			checkflag = "2";
		}
		return checkflag;
	}
	/**  
	* @Name: findElecUserByLogonName
	* @Description: 使用登录名获取用户的详细信息，用于首页登录的校验
	* @Create Date: 2018-01-22 
	* @Parameters: String name 登录名
	* @Return: ElecUser 存放用户详细信息
	*/
	@Override
	public ElecUser findElecUserByLogonName(String name) {
		String hqlWhere = " and o.logonName = ?";
		Object [] params = {name};
		List<ElecUser> list = elecUserDao.findCollectionByConditionNopage(hqlWhere, params, null);
		ElecUser elecUser = null;
		if(list!=null && list.size()>0){
			elecUser = list.get(0);
		}
		return elecUser;
	}
	/**  
	* @Name: findElecPopedomByLogonName
	* @Description: 使用登录名获取当前登录名所具有的权限
	* @Create Date: 2018-01-22
	* @Parameters: String name 登录名
	* @Return: String 用户存放该用户具有的权限
	*/
	public String findElecPopedomByLogonName(String name) {
		List<Object> list = elecUserDao.findElecPopedomByLogonName(name);
		StringBuffer buffer = new StringBuffer("");
		for(int i=0;list!=null && i<list.size();i++){
			Object object = list.get(i);
			buffer.append(object.toString());
		}
		return buffer.toString();
	}
	/**  
	* @Name: findElecRoleByLogonName
	* @Description: 使用登录名获取当前登录名所具有的角色
	* @Create Date: 2018-01-22 
	* @Parameters: String name 登录名
	* @Return: Hashtable<String, String> 存放角色的集合
	*/
	@Override
	public Hashtable<String, String> findElecRoleByLogonName(String name) {
		List<Object[]> list = elecUserDao.findElecRoleByLogonName(name);
		Hashtable<String, String> ht = null;
		if(list!=null && list.size()>0){
			ht = new Hashtable<String, String>();
			for(int i=0;i<list.size();i++){
				Object[] object = list.get(i);
				ht.put(object[0].toString(), object[1].toString());
			}
		}
		return ht;
	}
	/**  
	* @Name: getExcelFiledNameList
	* @Description: 获取excel的标题数据，放到ArrayList集合中
	* @Create Date: 2018-02-28 
	* @Parameters: 无
	* @Return: ArrayList （excel标题集数据）
	*/
	@Override
	public ArrayList getExcelFiledNameList() {
		String[] titles = {"登录名","用户姓名","性别","联系电话","是否在职"};
		// 数组转成ArrayList集合
		ArrayList filedName = new ArrayList();
		for(int i=0;i<titles.length;i++){
			String title = titles[i];
			filedName.add(title);
		}
		return filedName;
	}
	/**  
	* @Name: getExcelFiledDataList
	* @Description: 获取excel的列表数据，放到ArrayList集合中
	*          再实例化一个ArrayList filedData集合 filedData.add(dataList);
	* @Create Date: 2018-02-28 
	* @Parameters: 无
	* @Return: ArrayList （excel标题集数据）
	*/
	@Override
	public ArrayList getExcelFiledDataList(ElecUserForm elecUserForm) {
		//组织查询条件
		String hqlWhere = "";
		List<String> paramsList = new ArrayList<String>();
		if(elecUserForm!=null && elecUserForm.getUserName()!=null && !elecUserForm.getUserName().equals("")){
			hqlWhere += " and o.userName like ?";
			paramsList.add("%" + elecUserForm.getUserName() + "%");
		}
		Object [] params = paramsList.toArray();
		//组织排序
		LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
		orderby.put("o.onDutyDate", "desc");
		List<ElecUser> list = elecUserDao.findCollectionByConditionNopage(hqlWhere, params, orderby);
		List<ElecUserForm> formList = this.elecUserPOListToVOList(list);
		//构造报表导出数据
		ArrayList filedData = new ArrayList();
		for(int i=0;formList!=null && i<formList.size();i++){
			ArrayList dataList = new ArrayList();
			ElecUserForm userForm = formList.get(i);
			//zhugeliang	诸葛亮	男	88886666	是
			dataList.add(userForm.getLogonName());
			dataList.add(userForm.getUserName());
			dataList.add(userForm.getSexID());
			dataList.add(userForm.getContactTel());
			dataList.add(userForm.getIsDuty());
			filedData.add(dataList);
		}
		return filedData;
	}
	/**
	 * @description 从excel中读取数据，存放到数据库表elec_user表中
	 * @create date 2018-02-28
	 * @param ElecUserForm elecUserForm 存放excel的流对象
	 * @return 无
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@Override
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	public void saveElecUserWithExcel(ElecUserForm elecUserForm) {
		try {
			// 获取文件流
			File file = elecUserForm.getFile();
			GenerateSqlFromExcel generater = new GenerateSqlFromExcel();
			// 获取excel中的数据
			ArrayList<String[]> arrayList = generater.generateStationBugSql(file);
			// 保存到数据库中
			MD5keyBean md5 = new MD5keyBean();
			// 集合转换为数组
			for(int i=0;arrayList!=null && i<arrayList.size();i++){
				String[] data = arrayList.get(i);
				//实例化PO对象，用PO对象进行保存
				ElecUser elecUser = new ElecUser();
				//登录名	密码	用户姓名	性别	所属单位	联系地址	是否在职
				elecUser.setLogonName(data[0].toString());
				elecUser.setLogonPwd(md5.getkeyBeanofStr(data[1].toString()));
				elecUser.setUserName(data[2].toString());
				elecUser.setSexID(data[3].toString());
				elecUser.setJctID(data[4].toString());
				elecUser.setContactTel(data[5].toString());
				elecUser.setIsDuty(data[6].toString());
//				elecUser.setBirthday(StringHelper.stringCoverDate(data[7].toString()));
				elecUserDao.save(elecUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @description 使用柱状图按照所属单位统计用户数量
	 * @create date 2018-03-04
	 * @param 无
	 * @return List<ElecUserForm>结果集对象
	 */
	@Override
	public List<ElecUserForm> findUserByChart() {
		// 返回Object数组
		List<Object[]> list = elecUserDao.findUserByChart();
		// PO对象转换成VO对象
		List<ElecUserForm> formList = this.userChartPOListToVOList(list);
		return formList;
	}
	// PO对象转换成VO对象
	private List<ElecUserForm> userChartPOListToVOList(List<Object[]> list) {
		List<ElecUserForm> fomrList = new ArrayList<ElecUserForm>();
		ElecUserForm elecUserForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			Object [] object = list.get(i);
			elecUserForm = new ElecUserForm();
			elecUserForm.setJctname(object[0].toString());
			elecUserForm.setJctcount(object[1].toString());
			fomrList.add(elecUserForm);
		}
		return fomrList;
	}
	
}
