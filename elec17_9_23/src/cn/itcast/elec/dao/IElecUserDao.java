package cn.itcast.elec.dao;

import java.util.List;

import cn.itcast.elec.common.ICommonDao;
import cn.itcast.elec.domain.ElecUser;

public interface IElecUserDao extends ICommonDao<ElecUser>{
	public static final String SERVICE_NAME="cn.itcast.elec.dao.impl.ElecUserDaoImpl";

	List<Object> findElecPopedomByLogonName(String name);

	List<Object[]> findElecRoleByLogonName(String name);

	List<Object[]> findUserByChart();

}
