package cn.itcast.elec.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.dao.IElecTextDao;
import cn.itcast.elec.domain.ElecText;

public class TestDao {
	//保存
	@Test
	public void saveElecText(){
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
		//得到接口对象
		IElecTextDao elecTextDao=(IElecTextDao)ac.getBean(IElecTextDao.SERVICE_NAME);
		//实例化po对象，赋值，保存
		ElecText elecText = new ElecText();
		elecText.setTextName("测试Dao名称");
		elecText.setTextDate(new Date());
		elecText.setTextRemark("测试Dao备注");
		elecTextDao.save(elecText);
		//需要在.cfg.xml中配置事务为自动提交模式，数据才能持久化到数据库中
	}
	//修改
	@Test
	public void update(){
		IElecTextDao elecTextDao=(IElecTextDao)ServiceProvider.getService(IElecTextDao.SERVICE_NAME);
		//实例化po对象，赋值，保存
		ElecText elecText = new ElecText();
		elecText.setTextID("4028aa8a60733a150160733a1e850001");
		elecText.setTextName("赵巍");
		elecText.setTextDate(new Date());
		elecText.setTextRemark("太原武警医院主治医生");
		elecTextDao.update(elecText);
		//需要在.cfg.xml中配置事务为自动提交模式，数据才能持久化到数据库中
	}
	//通过id查询对象
	@Test
	public void findObjectById(){
		IElecTextDao elecTextDao=(IElecTextDao)ServiceProvider.getService(IElecTextDao.SERVICE_NAME);
		//所有的基本类型都实现序列化接口Serializable，不论id是String类型还是int类型都可以查询
		Serializable id="4028aa8a60733a150160733a1e850001";//序列化类型
		ElecText elecText=elecTextDao.findObjectById(id);
		System.out.println(elecText.toString());
	}
	//通过id数组删除多个对象
	@Test
	public void deleteObjectByIDs(){
		IElecTextDao elecTextDao=(IElecTextDao)ServiceProvider.getService(IElecTextDao.SERVICE_NAME);
		
		Serializable[] ids={"402881ee5eb36a0b015eb36a0e540001","402881ee5eb350a1015eb350acb70001"};//序列化类型
		elecTextDao.deleteObjectByIDs(ids);
	}
	//通过集合对象删除
	@Test
	public void deleteObjectByCollection(){
		IElecTextDao elecTextDao=(IElecTextDao)ServiceProvider.getService(IElecTextDao.SERVICE_NAME);
		
		List<ElecText> list=new ArrayList<ElecText>();
		ElecText elecText1=new ElecText();
		elecText1.setTextID("402881ec5e9f8ddc015e9f8de6df0001");
		ElecText elecText2=new ElecText();
		elecText2.setTextID("402881ec5e9fbc4c015e9fbc593b0001");
		list.add(elecText1);
		list.add(elecText2);
		
		elecTextDao.deleteObjectByCollection(list);
	}
}
