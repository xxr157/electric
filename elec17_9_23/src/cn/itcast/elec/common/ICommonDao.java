package cn.itcast.elec.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import cn.itcast.elec.domain.ElecUser;
import cn.itcast.elec.util.PageInfo;

public interface ICommonDao<T> {
	//保存
	public void save(T entity);
	//修改
	void update(T entity);
	//查询
	T findObjectById(Serializable id);
	//通过id数组删除多个对象(三个点表示数组)
	void deleteObjectByIDs(Serializable... id);
	//通过集合对象删除
	void deleteObjectByCollection(Collection<T> entities);
	//通过条件查询对象集合，无分页
	List<T> findCollectionByConditionNopage(String hqlWhere,
			Object[] params, LinkedHashMap<String, String> orderby);
	// 使用集合形式批量保存
	void saveObjectByCollection(Collection<T> entities);
	// 2018-02-27修改：添加分页功能
	List<T> findCollectionByConditionWithPage(String hqlWhere,
			Object[] params, LinkedHashMap<String, String> orderby,
			PageInfo pageInfo);
}
