package cn.itcast.elec.container;

import org.apache.commons.lang.xwork.StringUtils;
/**
 * 自定义容器
 * 可用于控制层调用业务层
 * @author Administrator
 *
 */
public class ServiceProvider {
//	static ServiceProviderCord spc;
	//只加载一次beans.xml文件
	static{
//		spc=new ServiceProviderCord();
//		spc.load("beans.xml");
		ServiceProviderCord.load("beans.xml");
	}
	//获取业务层实现类
	public static Object getService(String serviceName){
		//如果业务名称为空
		if(StringUtils.isBlank(serviceName)){
			throw new RuntimeException("当前业务名称为空！");
		}
		//如果有业务名称
		Object obj=null;
		if(ServiceProviderCord.ac.containsBean(serviceName)){
			obj=ServiceProviderCord.ac.getBean(serviceName);
		}
		if(obj==null){
			throw new RuntimeException("当前业务名称不存在！");
		}
		return obj;
	}
	
}
