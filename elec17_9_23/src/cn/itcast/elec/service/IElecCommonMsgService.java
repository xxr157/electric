package cn.itcast.elec.service;

import java.util.List;

import cn.itcast.elec.web.form.ElecCommonMsgForm;

public interface IElecCommonMsgService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecCommonMsgServiceImpl";

	List<ElecCommonMsgForm> findElecCommonMsgList();

	void saveElecCommonMsg(ElecCommonMsgForm elecCommonMsgForm);

	List<ElecCommonMsgForm> findElecCommonMsgByCurruntDate();
	
}
