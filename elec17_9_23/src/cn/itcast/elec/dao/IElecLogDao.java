package cn.itcast.elec.dao;


import cn.itcast.elec.common.ICommonDao;
import cn.itcast.elec.domain.ElecLog;

public interface IElecLogDao extends ICommonDao<ElecLog>{
	public static final String SERVICE_NAME="cn.itcast.elec.dao.impl.ElecLogDaoImpl";


}
