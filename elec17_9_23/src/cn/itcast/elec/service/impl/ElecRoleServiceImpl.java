package cn.itcast.elec.service.impl;


import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecRolePopedomDao;
import cn.itcast.elec.dao.IElecUserRoleDao;
import cn.itcast.elec.domain.ElecRolePopedom;
import cn.itcast.elec.domain.ElecUserRole;
import cn.itcast.elec.service.IElecRoleService;
import cn.itcast.elec.util.XmlObject;
import cn.itcast.elec.web.form.ElecRoleForm;
import cn.itcast.elec.web.form.ElecUserForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecRoleService.SERVICE_NAME)
public class ElecRoleServiceImpl implements IElecRoleService{
	// 引入用户角色
	@Resource(name=IElecUserRoleDao.SERVICE_NAME)
	private IElecUserRoleDao elecUserRoleDao;
	// 引入角色权限
	@Resource(name=IElecRolePopedomDao.SERVICE_NAME)
	private IElecRolePopedomDao elecRolePopedomDao;
	/**
	 * @description 从Function.xml配置文件中获取权限集合,存放到xmlObject对象中
	 * @create date 2017-1-6
	 * @return List<XmlObject>权限集合
	 */
	@Override
	public List<XmlObject> readXmlObject() {
		ServletContext servletContext = ServletActionContext.getServletContext();
		String realPath = servletContext.getRealPath("/WEB-INF/classes/Function.xml");
		File file = new File(realPath);
		List<XmlObject> xmlList = new ArrayList<XmlObject>();
		// 使用dom4j读取器配置文件
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(file);
			Element element = document.getRootElement();
			XmlObject xmlObject = null;
			/**
			 * Function:对应配置文件中的Function元素节点
			 * FunctionCode：对应配置文件中Function元素节点下的FunctionCode元素节点
			 * FunctionName：对应配置文件中Function元素节点下的FunctionName元素节点
			 * ParentCode：对应配置文件中Function元素节点下的ParentCode元素节点
			 * ParentName：对应配置文件中Function元素节点下的ParentName元素节点
			 */
			for(Iterator<Element> iter = element.elementIterator("Function");iter.hasNext();){
				Element xmlElement = iter.next();
				xmlObject = new XmlObject();
				xmlObject.setCode(xmlElement.elementText("FunctionCode"));
				xmlObject.setName(xmlElement.elementText("FunctionName"));
				xmlObject.setParentCode(xmlElement.elementText("ParentCode"));
				xmlObject.setParentName(xmlElement.elementText("ParentName"));
				xmlList.add(xmlObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xmlList;
	}
	/**
	 * @description 使用角色id查询该角色下具有的权限，并与系统中所有的权限进行匹配
	 * @create date 2018-01-22
	 * @param String roleID 角色ID
	 * @return List<XmlObject> 权限集合（匹配完成）
	 */
	@Override
	public List<XmlObject> readEditXml(String roleID) {
		// 通过角色id查询该角色下具有的权限
		ElecRolePopedom elecRolePopedom = elecRolePopedomDao.findObjectById(roleID);
		String popedom = "";
		if(elecRolePopedom!=null){
			popedom = elecRolePopedom.getPopedomCode();
		}
		// 与系统中所有的权限进行匹配
		List<XmlObject> list = this.readXmlByPopedom(popedom);
		return list;
	}
	/*
	 *  读取配置文件，获取系统中所有权限，与当前角色进行匹配
	 *  * 如果匹配不成功，设置flag = 0，表示该角色不具有的权限，则页面中权限复选框不被选中
	 *  * 如果匹配成功，设置flag = 1，表示该角色具有此权限，则页面中的权限复选框被选中
	 */
	private List<XmlObject> readXmlByPopedom(String popedom) {
		List<XmlObject> list = new ArrayList<XmlObject>();
		List<XmlObject> xmlList = this.readXmlObject();
		for(int i=0;xmlList!=null && i<xmlList.size();i++){
			XmlObject object = xmlList.get(i);
			//表示当前权限被选中
			if(popedom.contains(object.getCode())){
				object.setFlag("1");
			}
			else{
				object.setFlag("0");
			}
			list.add(object);
		}
		return list;
	}
	/**
	 * @description 查询用户列表集合，获取系统中所有的用户，并与该角色具有的用户进行匹配
	*               * 如果匹配不成功，设置flag = 0，表示该角色不拥有的用户，则页面中用户复选框不被选中
	*               * 如果匹配成功，设置flag = 1，表示该角色拥有此用户，则页面中的用户复选框被选中
	 * @create date 2018-01-22
	 * @param String roleID 角色ID
	 * @return List<XmlObject> 用户集合（匹配完成）
	 */
	@Override
	public List<ElecUserForm> findElecUserListByRoleID(String roleID) {
		List<Object []> list = elecUserRoleDao.findElecUserListByRoleID(roleID);
		// 将获取到的用户列表信息从Object对象转换成VO对象
		List<ElecUserForm> formList = this.elecUserRoleObjectListToVOList(list);
		return formList;
	}
	// 将获取到的用户列表信息从Object对象转换成VO对象
	private List<ElecUserForm> elecUserRoleObjectListToVOList(
			List<Object[]> list) {
		List<ElecUserForm> formList = new ArrayList<ElecUserForm>();
		ElecUserForm elecUserForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			Object[] objects = list.get(i);
			elecUserForm = new ElecUserForm();
			elecUserForm.setFlag(objects[0].toString());
			elecUserForm.setUserID(objects[1].toString());
			elecUserForm.setUserName(objects[2].toString());
			elecUserForm.setLogonName(objects[3].toString());
			formList.add(elecUserForm);
		}
		return formList;
	}
	/**
	 * @description 保存角色和权限的关联表
	 * 				  保存用户和角色的关联表
	 * @create date 2018-01-22
	 * @param ElecRoleForm elecRoleForm 存放角色ID，角色code数组，用户id数组
	 * @return 无
	 */
	
	@Override
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	public void saveRole(ElecRoleForm elecRoleForm) {
		// 保存角色和权限的关联表
		this.saveRolePopedom(elecRoleForm);
		// 保存用户和角色的关联表
		this.saveUserRole(elecRoleForm);
	}
	// 保存或更新角色和权限的关联表
	private void saveRolePopedom(ElecRoleForm elecRoleForm) {
		// 角色id
		String roleid = elecRoleForm.getRoleid();
		// 权限code集合
		String[] selectoper = elecRoleForm.getSelectoper();
		StringBuffer popedom = new StringBuffer();
		for(int i=0;selectoper!=null && i<selectoper.length;i++){
			popedom.append(selectoper[i]);
		}
		// 使用角色id查询角色和权限的关联表
		ElecRolePopedom elecRolePopedom = elecRolePopedomDao.findObjectById(roleid);
		// 角色和权限关联表中存在该角色的记录，此时执行update的操作
		if(elecRolePopedom!=null){
			elecRolePopedom.setPopedomCode(popedom.toString());
			elecRolePopedomDao.update(elecRolePopedom);
		}
		// 角色和权限关联表中不存在该角色的记录，此时执行save的操作
		else{
			elecRolePopedom = new ElecRolePopedom();
			elecRolePopedom.setRoleID(roleid);
			elecRolePopedom.setPopedomCode(popedom.toString());
			elecRolePopedomDao.save(elecRolePopedom);
		}
	}
	// 保存用户和角色的关联表
	private void saveUserRole(ElecRoleForm elecRoleForm) {
		// 角色id
		String roleid = elecRoleForm.getRoleid();
		// 用户id集合
		String[] selectuser = elecRoleForm.getSelectuser();
		/**
		 * 以roleID作为条件，查询用户和角色的关联表，获取用户和角色关联的集合对象
		 */
		String hqlWhere = " and o.roleID = ?";
		Object [] params = {roleid};
		List<ElecUserRole> entities = elecUserRoleDao.findCollectionByConditionNopage(hqlWhere, params, null);
		/**
		 * 以roleID作为条件，删除用户和角色的关联表
		 */
		elecUserRoleDao.deleteObjectByCollection(entities);
		//新增用户和角色的关联表
		List<ElecUserRole> list = new ArrayList<ElecUserRole>();
		for(int i=0;selectuser!=null && i<selectuser.length;i++){
			String userID = selectuser[i];
			ElecUserRole elecUserRole = new ElecUserRole();
			elecUserRole.setUserID(userID);
			elecUserRole.setRoleID(roleid);
			list.add(elecUserRole);
			//elecUserRoleDao.save(elecUserRole);
		}
		elecUserRoleDao.saveObjectByCollection(list);
	}
}
