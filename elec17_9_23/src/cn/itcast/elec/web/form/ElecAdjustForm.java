package cn.itcast.elec.web.form;

/**
 * vo值对象，对应页面表单的属性值
 * 用于首页显示
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecAdjustForm implements java.io.Serializable{
	private String name;
	private String password;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
