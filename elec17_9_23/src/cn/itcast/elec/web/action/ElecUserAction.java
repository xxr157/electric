package cn.itcast.elec.web.action;


import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.service.IElecLogService;
import cn.itcast.elec.service.IElecSystemDDLService;
import cn.itcast.elec.service.IElecUserService;
import cn.itcast.elec.util.ChartUtils;
import cn.itcast.elec.util.ExcelFileGenerator;
import cn.itcast.elec.web.form.ElecSystemDDLForm;
import cn.itcast.elec.web.form.ElecUserForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * @author Administrator
 *
 */
public class ElecUserAction extends BaseAction implements ModelDriven<ElecUserForm>{
	private ElecUserForm elecUserForm=new ElecUserForm();
	// 写在父类中了
	//private HttpServletRequest request=null;
	// 控制层调用业务层
	private IElecUserService elecUserService=(IElecUserService)ServiceProvider.getService(IElecUserService.SERVICE_NAME);
	// 引入数据字典
	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);
	// 调用日志管理业务层
	private IElecLogService elecLogService = (IElecLogService)ServiceProvider.getService(IElecLogService.SERVICE_NAME);
	
	@Override
	public ElecUserForm getModel() {
		return elecUserForm;
	}
	/**
	 * @description 查询所有用户信息
	 * @create date 2017-12-29
	 * @param 无
	 * @return String home跳转到userIndex.jsp
	 */
	public String home(){
		// begin:2018-02-27修改：添加分页功能
//		List<ElecUserForm> list = elecUserService.findElecUserList(elecUserForm);
		List<ElecUserForm> list = elecUserService.findElecUserList(elecUserForm,request);
		request.setAttribute("userList", list);
		//使用initflag标识，判断当前跳转的userIndex.jsp还是userList.jsp
		String initflag = request.getParameter("initflag");
		if(initflag!=null && initflag.equals("1")){
			return "userlist";
		}
		// end
		return "home";
	}
	/**
	 * @description 添加用户
	 * @create date 2017-12-30
	 * @param 无
	 * @return String add跳转到userAdd.jsp
	 */
	public String add(){
		/*
		 * 使用数据类型查询对应数据类型下的数据项编号和数据项名称
		 * 查询性别、所属单位、是否在职
		 */
		this.initSystemDDL();
		return "add";
	}
	/*
	 * 初始化新增和编辑页面中的数据字典项
	 *  使用数据类型查询对应数据类型下的数据项编号和数据项名称
	 *  查询性别、所属单位、是否在职
	 */
	private void initSystemDDL() {
		List<ElecSystemDDLForm> sexList = elecSystemDDLService.findElecSystemDDLListByKeyword("性别");
		List<ElecSystemDDLForm> jctList = elecSystemDDLService.findElecSystemDDLListByKeyword("所属单位");
		List<ElecSystemDDLForm> isDutyList = elecSystemDDLService.findElecSystemDDLListByKeyword("是否在职");
		request.setAttribute("sexList", sexList);
		request.setAttribute("jctList", jctList);
		request.setAttribute("isDutyList", isDutyList);
	}
	/**
	 * @description 保存用户信息
	 * @create date 2017-12-30
	 * @param 无
	 * @return String save重定向到userIndex.jsp
	 */
	public String save(){
		// 保存页面信息
		elecUserService.saveElecUser(elecUserForm);
		/*****begin：2018-02-25添加：将新增和修改用户的信息添加到日志表中****/
		// 通过是否有userID来判断是新增用户该是修改用户
		if(elecUserForm.getUserID()!=null && !"".equals(elecUserForm.getUserID())){
			// 保存修改用户日志信息
			elecLogService.saveElecLog(request, "用户管理模块：修改用户【"+elecUserForm.getUserName()+"】的信息");
		}else{
			// 保存新增用户日志信息
			elecLogService.saveElecLog(request, "用户管理模块：新增用户【"+elecUserForm.getUserName()+"】的信息");
		}
		/***** end添加日志*****/
		// 获取userEdit.jsp中roleflag值
		String roleflag = request.getParameter("roleflag");
		// roleflag=1条跳转到userEdit.jsp,roleflag=""跳转都userIndex.jsp
		if(roleflag!=null && roleflag.equals("1")){
			// 跳转到userEdit.jsp页面
			return edit();
		}
		return "list";
	}
	/**
	 * @description 编辑用户信息
	 * @create date 2017-12-31
	 * @param 无
	 * @return String edit跳转到userEdit.jsp
	 */
	public String edit(){
		elecUserForm = elecUserService.findElecUser(elecUserForm);
		// 使用值栈的形式传递elecUserForm对象，注释原因见ElecUserServiceImpl.java
//		ActionContext.getContext().getValueStack().push(elecUserForm);
		// 初始化数据字典项
		this.initSystemDDL();
		/*
		 * 使用viewflag字段判断当前用户操作的是编辑还是查询明细
		 *  如果viewflag==null，则说明用户操作的是编辑操作
		 *  如果viewflag==1，则说明用户操作的是查询明细操作
		 */
		// 获取页面viewflag值
		String viewflag = elecUserForm.getViewflag();
		// 保存到request域中
		request.setAttribute("viewflag", viewflag);
		/*
		 * 2018-02-18日修改，判断点击左侧”用户管理”的连接
		 *    * 如果当前操作人是系统管理员或者是高级管理员的时候，则点击“用户管理”的时候
	     *      跳转到userIndex.jsp，可以查看用户列表信息
	     *    * 如果当前操作人不是系统管理员或者是高级管理员的时候，则点击“用户管理”的时候
	     *      需要跳转到userEdit.jsp，可以对当前登录人进行编辑并保存
	     * 在userEdit.jsp中添加roleflag隐藏字段
		 */
		String roleflag = elecUserForm.getRoleflag();
		request.setAttribute("roleflag", roleflag);
		return "edit";
	}
	/**
	 * @description 删除用户信息
	 * @create date 2017-1-4
	 * @param 无
	 * @return String delete重定向到userIndex.jsp
	 */
	public String delete(){
		elecUserService.deleteElecUser(elecUserForm);
		return "list";
	}
	/**
	 * @description 导出excel的报表数据
	 * @create date 2018-02-28
	 * @param 无
	 * @return 
	 */
	public String export(){
		// 获取要导出的表头，存放到ArrayList对象中（登录名，用户名，性别，联系电话，是否在职）
		ArrayList filedName = elecUserService.getExcelFiledNameList();
		// 获取列表数据，存放到ArrayList dataList中，在实例化一个ArrayList filedData集合 filedData.add(dataList);
		ArrayList filedData = elecUserService.getExcelFiledDataList(elecUserForm);
		try {
			// 获取输出流
			OutputStream out = response.getOutputStream();
			// 重置输出流
			response.reset();
			// 设置导出报表的形式：excel
			response.setContentType("application/vnd.ms-excel");
			ExcelFileGenerator generator = new ExcelFileGenerator(filedName,filedData);
			generator.expordExcel(out);
			// 设置输出形式
			System.setOut(new PrintStream(out));
			// 刷新输出流
			out.flush();
			// 关闭输出流
			if(out!=null){
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * @description 跳转到导入excel的报表页面
	 * @create date 2018-02-28
	 * @param 无
	 * @return String 跳转到userImport.jsp
	 */
	public String importpage(){
		return "importpage";
	}
	/**
	 * @description 从excel中读取数据，存放到数据库中
	 * @create date 2018-02-28
	 * @param 无
	 * @return String 跳转到userImport.jsp
	 */
	public String importdata(){
		elecUserService.saveElecUserWithExcel(elecUserForm);
		return "importdata";
	}
	/**
	 * @description 使用柱状图按照所属单位统计用户数量	
	 * @create date 2018-03-04
	 * @param 无
	 * @return String 跳转到userReport.jsp
	 */
	public String chart(){
		// 查询用户所属单位及其人数
		List<ElecUserForm> list = elecUserService.findUserByChart();
		// 生成图片名称
		String filename = ChartUtils.getUserBarChart(list);
		request.setAttribute("filename", filename);
		return "chart";
	}
	
}
