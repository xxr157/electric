package cn.itcast.elec.web.action;


import java.util.List;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.service.IElecLogService;
import cn.itcast.elec.web.form.ElecLogForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * @author Administrator
 *
 */
public class ElecLogAction extends BaseAction implements ModelDriven<ElecLogForm>{
	private ElecLogForm elecLogForm=new ElecLogForm();
	//写在父类中了
	//private HttpServletRequest request=null;
	//控制层调用业务层
	IElecLogService elecLogService=(IElecLogService)ServiceProvider.getService(IElecLogService.SERVICE_NAME);

	@Override
	public ElecLogForm getModel() {
		return elecLogForm;
	}
	
	/**
	 * @description 查询日志列表信息
	 * @param 无
	 * @return String home跳转到logIndex.jsp
	 * @create date 2018-02-25
	 */
	public String home(){
		List<ElecLogForm> logList = elecLogService.findElecLogListByCondition(elecLogForm);
		request.setAttribute("logList", logList);
		return "home";
	}
	
	/**
	 * @description 删除查询得到的日志信息
	 * @param 无
	 * @return String delete重定向到logIndex.jsp
	 * @create date 2018-02-25
	 */
	public String delete(){
		// 第三种方式：获取logID数组
//		String[] logids = request.getParameterValues("logID");
		elecLogService.deleteElecLogByLogIds(elecLogForm);
		return "delete";
	}
}
