package cn.itcast.elec.domain;

/**
 * 用户角色信息
 * po持久层对象，对应数据库表Elec_User_Role
 * @author Administrator
 * 
 * 问题:当重启服务器后刷新已登录用户页面会退出登录，已不在是登录状态
 * 解决问题：服务器关闭后会在apache-tomcat-7.0.42\work\Catalina\localhost\OA17_8_15目录下生成一个SESSIONS.ser文件以保存session信息，
 *   虽然user是保存在session中的，但我们都User类不能被序列化，服务器关闭不会将用户登录信息写入SESSIONS.ser文件中
 *   所以需要实现序列化接口java.io.Serializable，才能被写入文件中
 *
 */

@SuppressWarnings("serial")
public class ElecUserRole implements java.io.Serializable {
	private Integer seqID; // 主键ID
	private String userID; // 用户ID
	private String roleID;   // 角色ID
	private String remark; // 备注
	
	public Integer getSeqID() {
		return seqID;
	}
	public void setSeqID(Integer seqID) {
		this.seqID = seqID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
