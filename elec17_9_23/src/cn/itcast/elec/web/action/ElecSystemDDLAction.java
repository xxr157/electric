package cn.itcast.elec.web.action;


import java.util.List;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.domain.ElecSystemDDL;
import cn.itcast.elec.service.IElecSystemDDLService;
import cn.itcast.elec.web.form.ElecSystemDDLForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * 数据字典
 * @author Administrator
 *
 */
public class ElecSystemDDLAction extends BaseAction implements ModelDriven<ElecSystemDDLForm>{
	private ElecSystemDDLForm elecSystemDDLForm=new ElecSystemDDLForm();
	//写在父类中了
	//private HttpServletRequest request=null;
	//控制层调用业务层
	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);

	@Override
	public ElecSystemDDLForm getModel() {
		return elecSystemDDLForm;
	}
	/**
	 * @description 查询数据字典中所有数据类型，去掉重复值（distinct）
	 * @create date 2017-12-26
	 * @return String home 跳转到dictionaryIndex.jsp页面
	 */
	public String home(){
		List<ElecSystemDDLForm> list = elecSystemDDLService.findKeyWord();
		request.setAttribute("systemList", list);
		return "home";
	}
	/**
	 * @description 根据选中的数据类型，跳转到编辑此数据类型的页面
	 * @create date 2017-12-26
	 * @return String edit 跳转到dictionaryEdit.jsp页面
	 */
	public String edit(){
		// 获取页面所选数据类型
		String keyword = elecSystemDDLForm.getKeyword();
		// 通过keyword查询数据字典中对应其他数据项
		List<ElecSystemDDLForm> list = elecSystemDDLService.findElecSystemDDLListByKeyword(keyword);
		request.setAttribute("systemList", list);
		return "edit";
	}
	/**
	 * @description 保存数据字典
	 * @create date 2017-12-27
	 * @return String save 重定向到actingIndex.jsp
	 */
	public String save(){
		elecSystemDDLService.saveElecSystemDDL(elecSystemDDLForm);
		return "save";
	}
	

}
