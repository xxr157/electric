package cn.itcast.elec.web.action;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.dao.IElecLogDao;
import cn.itcast.elec.domain.ElecUser;
import cn.itcast.elec.service.IElecCommonMsgService;
import cn.itcast.elec.service.IElecLogService;
import cn.itcast.elec.service.IElecUserService;
import cn.itcast.elec.util.LoginUtil;
import cn.itcast.elec.util.MD5keyBean;
import cn.itcast.elec.web.form.ElecCommonMsgForm;
import cn.itcast.elec.web.form.ElecMenuForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 处理首页登录
 * @author Administrator
 *
 */
public class ElecMenuAction extends BaseAction implements ModelDriven<ElecMenuForm>{
	// 引入站点运行情况业务层
	private IElecCommonMsgService elecCommonMsgService = (IElecCommonMsgService)ServiceProvider.getService(IElecCommonMsgService.SERVICE_NAME);
	// 注入用户信息
	private IElecUserService elecUserService = (IElecUserService)ServiceProvider.getService(IElecUserService.SERVICE_NAME);
	// 调用日志管理业务层
	private IElecLogService elecLogService = (IElecLogService)ServiceProvider.getService(IElecLogService.SERVICE_NAME);
	
	private ElecMenuForm elecMenuForm=new ElecMenuForm();
	// 使用log4j
	Log log = (Log) LogFactory.getLog(ElecMenuAction.class);
	
	@Override
	public ElecMenuForm getModel() {
		return elecMenuForm;
	}
	/**
	 *  menu/index.jsp中首页登录
	 * @description 验证登录名和密码,成功跳转到home.jsp,失败转向index.jsp
	 * @create date 2018-02-16
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String home() throws UnsupportedEncodingException{
		/* begin 校验验证码*/
		boolean flag = LoginUtil.checkNumber(request);
		if(!flag){
			this.addFieldError("error", "验证码输入有误，请重新输入！");
			return "error";
		}
		/* end 校验验证码*/
		// 获取当前登录名和密码
		String name = elecMenuForm.getName();
		String password = elecMenuForm.getPassword();
		MD5keyBean md5 = new MD5keyBean();
		String md5password = md5.getkeyBeanofStr(password);
		// 使用登录名查询数据库，获取用户的详细信息
		ElecUser elecUser = elecUserService.findElecUserByLogonName(name);
		if(elecUser==null){
			// 使用filederror.ftl标签传递错误信息
			this.addFieldError("error", "您当前输入的登录名或密码有误或不存在!");
			return "error";
		}
		if(password==null || password.equals("") || !elecUser.getLogonPwd().equals(md5password)){
			this.addFieldError("error", "您当前输入的登录名或密码有误或不存在!");
			return "error";
		}
		request.getSession().setAttribute("globle_user", elecUser);
		//获取当前登录名所具有的权限
		String popedom = elecUserService.findElecPopedomByLogonName(name);
		if(popedom==null || "".equals(popedom)){
			this.addFieldError("error", "当前登录名没有分配权限，请与管理员联系");
			return "error";
		}
		request.getSession().setAttribute("globle_popedom", popedom);
		//获取当前登录名所具有的角色
		Hashtable<String, String> ht = elecUserService.findElecRoleByLogonName(name);
		if(ht==null){
			this.addFieldError("error", "当前登录名没有分配角色，请与管理员联系");
			return "error";
		}
		request.getSession().setAttribute("globle_role", ht);
		/****begin 添加记住我功能，记住当前用户名和密码****/
		LoginUtil.remeberMeByCookie(request,response);
		/****end 添加记住我功能，记住当前用户名和密码****/
		
		// begin:2018-02-20 使用log4j添加日志管理模块，维护系统性能安全
		/********** 使用log4j生成*.txt方式**********/
        //Log log = (Log) LogFactory.getLog(ElecMenuAction.class);
//		java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
//		String d = date.toString();
//		log.info("用户名：【"+elecUser.getUserName()+"】登录系统！时间是："+d);
		/**********8使用数据库表的形式***********/
		// 保存日志信息(通用方法)
		elecLogService.saveElecLog(request,"登录模块：当前用户：【"+elecUser.getUserName()+"】登录系统");
		// end 添加日志管理模块
		return "home";
	}
	/*
	 * home.jsp中
	 */
	public String title(){
		return "title";
	}
	public String left(){
		return "left";
	}
	public String change1(){
		return "change1";
	}
	public String loading(){
		return "loading";
	}
	/*
	 * loading.jsp中
	 */
	// 校准提醒
	public String alermXZ(){
		return "alermXZ";
	}
	// 检修提醒
	public String alermJX(){
		return "alermJX";
	}
	/**
	 * @description 查询当天站点运行情况
	 * @create date 2017-12-24
	 * @return 跳转到alermZD.jsp
	 */
	public String alermZD(){
		List<ElecCommonMsgForm> list = elecCommonMsgService.findElecCommonMsgByCurruntDate();
		request.setAttribute("commonList", list);
		return "alermZD";
	}
	/**
	 * @description 查询当天设备运行情况
	 * @create date 2017-12-24
	 * @return 跳转到alermSB.jsp
	 */
	public String alermSB(){
		List<ElecCommonMsgForm> list = elecCommonMsgService.findElecCommonMsgByCurruntDate();
		request.setAttribute("commonList", list);
		return "alermSB";
	}
	// 验收提醒
	public String alermYS(){
		return "alermYS";
	}
	/**  
	* @Name: logout
	* @Description: 重新回退到登录页面
	* @Create Date: 2018-01-22
	* @Parameters: 无
	* @Return: String logout 跳转到index.jsp
	*/
	public String logout(){
		//清空session
		request.getSession().invalidate();
		return "logout";
	}
	
}
