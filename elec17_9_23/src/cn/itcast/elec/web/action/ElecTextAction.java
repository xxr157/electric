package cn.itcast.elec.web.action;


import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.domain.ElecText;
import cn.itcast.elec.service.IElecTextService;
import cn.itcast.elec.util.StringHelper;
import cn.itcast.elec.web.form.ElecTextForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * @author Administrator
 *
 */
public class ElecTextAction extends BaseAction implements ModelDriven<ElecTextForm>{
	private ElecTextForm elecTextForm=new ElecTextForm();
	//写在父类中了
	//private HttpServletRequest request=null;
	//控制层调用业务层
	IElecTextService elecTextService=(IElecTextService)ServiceProvider.getService(IElecTextService.SERVICE_NAME);

	@Override
	public ElecTextForm getModel() {
		return elecTextForm;
	}
	public String save(){
//		System.out.println(request.getParameter("textDate"));
		/*
		 * VO对象转换成po对象
		 */
		ElecText elecText=new ElecText();//实例化po对象
		elecText.setTextName(elecTextForm.getTextName());
		//写一个util1工具类实现将String类型日期转换成Date型
		elecText.setTextDate(StringHelper.stringCoverDate(elecTextForm.getTextDate()));
		elecText.setTextRemark(elecTextForm.getTextRemark());
		/*
		 * 业务层调用dao层采用的注解的形式，如果控制层调用业务层也采用这种形式，则不利于项目维护
		 * 所以采用分离的形式，控制层调用业务层使用spring容器注入的形式
		 */
		//下面这两行代码独立使用static代码块封装起来，这样每次调用只需加载一次，提高效率（container包中）
//		ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
//		IElecTextService elecTextService=(IElecTextService)ac.getBean(IElecTextService.SERVICE_NAME);
		//放到方法外，成员变量
		//IElecTextService elecTextService=(IElecTextService)ServiceProvider.getService(IElecTextService.SERVICE_NAME);
		
		elecTextService.saveElecText(elecText);
		return "save";
	}

}
