package cn.itcast.elec.domain;

import java.util.Date;

/**
 * po持久层对象，对应数据库表elec_log
 * @author Administrator
 * 
 * 问题:当重启服务器后刷新已登录用户页面会退出登录，已不在是登录状态
 * 解决问题：服务器关闭后会在apache-tomcat-7.0.42\work\Catalina\localhost\OA17_8_15目录下生成一个SESSIONS.ser文件以保存session信息，
 *   虽然user是保存在session中的，但我们都User类不能被序列化，服务器关闭不会将用户登录信息写入SESSIONS.ser文件中
 *   所以需要实现序列化接口java.io.Serializable，才能被写入文件中
 *
 */

@SuppressWarnings("serial")
public class ElecLog implements java.io.Serializable {
	private String logID;       // 主键ID
	private String ipAddress;   // IP地址
	private String opeName;     // 操作人
	private Date opeTime;       // 操作时间
	private String details;     // 操作明细
	
	public String getLogID() {
		return logID;
	}
	public void setLogID(String logID) {
		this.logID = logID;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getOpeName() {
		return opeName;
	}
	public void setOpeName(String opeName) {
		this.opeName = opeName;
	}
	public Date getOpeTime() {
		return opeTime;
	}
	public void setOpeTime(Date opeTime) {
		this.opeTime = opeTime;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
	
	
}
