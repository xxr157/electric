<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <title>load</title>
    <link href="${pageContext.request.contextPath }/css/Style.css" type="text/css" rel="stylesheet" />
   </head>
  
  <body>
    <table width="100%" border="0" id="table8">
		<s:if test="#request.commonList!=null">
			<s:iterator value="%{#request.commonList}" var="common">
				<tr>
					<td align="left" valign="middle"  style="word-break: break-all">
					<span class="style1">
						<!-- 输出当前站点情况 -->
						<!--  这种输出方式不会解析jsp中的换行符号<br>
						<s:property value="%{#common.stationRun}"/>
						-->
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						${common.stationRun }
					</span></td>
				</tr>	
			</s:iterator>	
		</s:if>	
	</table>
  </body>
</html>