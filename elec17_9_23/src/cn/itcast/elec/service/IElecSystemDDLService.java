package cn.itcast.elec.service;

import java.util.List;

import cn.itcast.elec.web.form.ElecCommonMsgForm;
import cn.itcast.elec.web.form.ElecSystemDDLForm;

public interface IElecSystemDDLService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecSystemDDLServiceImpl";

	List<ElecSystemDDLForm> findKeyWord();

	List<ElecSystemDDLForm> findElecSystemDDLListByKeyword(String keyword);

	void saveElecSystemDDL(ElecSystemDDLForm elecSystemDDLForm);

}
