package cn.itcast.elec.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

public class LoginUtil {
	/**
	 * 首页添加验证码功能
	 * @param request
	 * @return
	 * @create date 2018-02-19
	 */
	public static boolean checkNumber(HttpServletRequest request) {
		// 获取image.jsp存储到session中的图片验证码
		HttpSession session = request.getSession(false);
		if(session==null){
			return false;
		}
		String CHECK_NUMBER_KEY = (String) session.getAttribute("CHECK_NUMBER_KEY");
		if(StringUtils.isBlank(CHECK_NUMBER_KEY)){
			return false;
		}
		// 获取用户输入的验证码
		String checkNumber = request.getParameter("checkNumber");
		if(StringUtils.isBlank(checkNumber)){
			return false;
		}
		return CHECK_NUMBER_KEY.equalsIgnoreCase(checkNumber);
		
	}
	/**
	 * 首页添加记住我功能
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException 
	 * @create date 2018-02-19
	 */
	public static void remeberMeByCookie(HttpServletRequest request,
			HttpServletResponse response) throws UnsupportedEncodingException {
		// 获取用户输入的登录名和密码
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		// 创建2个cookie，用来存放用户名和密码
		//处理Cookie中存在的中文字符
		String codeName = URLEncoder.encode(name, "UTF-8");
		Cookie nameCookie = new Cookie("name",codeName);
		Cookie passwordCookie = new Cookie("password",password);
		//设置Cookie的有效路径，有效路径定义为项目的根路径
		nameCookie.setPath(request.getContextPath()+"/");
		passwordCookie.setPath(request.getContextPath()+"/");
		/**
		 * 从页面中获取记住我的复选框的值，
		 *    * 如果有值，设置Cookie的有效时长
		 *    * 如果没有值，清空Cookie的有效时长
		 * <input type="checkbox" name="remeberMe" id="remeberMe" value="yes">
		 */
		String remeberMe = request.getParameter("remeberMe");
		//如果用户选中记住我复选框，则设置Cookie的有效时长
		if(remeberMe!=null && remeberMe.equals("yes")){
			// 保存一周时间
			nameCookie.setMaxAge(7*24*60*60);
			passwordCookie.setMaxAge(7*24*60*60);
		}
		//否则，清空Cookie的有效时长
		else{
			nameCookie.setMaxAge(0);
			passwordCookie.setMaxAge(0);
		}
		//将2个Cookie的对象存放到response对象
		response.addCookie(nameCookie);
		response.addCookie(passwordCookie);
	}

}
