package cn.itcast.elec.service;

import java.util.List;

import cn.itcast.elec.domain.ElecText;
import cn.itcast.elec.web.form.ElecTextForm;

public interface IElecTextService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecTextServiceImpl";
	//保存
	public void saveElecText(ElecText elecText);
	////通过条件查询对象集合，无分页
	public List<ElecText> findCollectionByConditionNopage(
			ElecTextForm elecTextForm);
}
