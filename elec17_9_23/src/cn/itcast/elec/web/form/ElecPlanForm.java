package cn.itcast.elec.web.form;

import java.util.Date;

/**
 * vo值对象，对应页面表单的属性值
 * vo对象与po对象的关系：
 * 	        相同点：都是javabean
 *     不同点：PO对象中的属性关联数据库的字段
 *            VO对象中的属性可以随意增加、修改、删除，对应的页面表单属性
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecPlanForm implements java.io.Serializable{

}
