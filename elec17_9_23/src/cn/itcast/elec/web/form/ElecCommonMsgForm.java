package cn.itcast.elec.web.form;

import java.util.Date;

/**
 * vo值对象，对应页面表单的属性值
 * vo对象与po对象的关系：
 * 	        相同点：都是javabean
 *     不同点：PO对象中的属性关联数据库的字段
 *            VO对象中的属性可以随意增加、修改、删除，对应的页面表单属性
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecCommonMsgForm implements java.io.Serializable{
	private String comID; // 主键ID
	private String stationRun; // 站点运行情况
	private String devRun; // 设备运行情况
	private String createDate; // 创建日期
	
	public String getComID() {
		return comID;
	}
	public void setComID(String comID) {
		this.comID = comID;
	}
	public String getStationRun() {
		return stationRun;
	}
	public void setStationRun(String stationRun) {
		this.stationRun = stationRun;
	}
	public String getDevRun() {
		return devRun;
	}
	public void setDevRun(String devRun) {
		this.devRun = devRun;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
}
