package cn.itcast.elec.test;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.domain.ElecText;
import cn.itcast.elec.service.IElecTextService;
import cn.itcast.elec.web.form.ElecTextForm;

public class TestService {
	@Test
	public void saveElecText(){
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
		//得到接口对象
		IElecTextService elecTextService=(IElecTextService)ac.getBean(IElecTextService.SERVICE_NAME);
		//实例化po对象，赋值，保存
		ElecText elecText = new ElecText();
		elecText.setTextName("张无忌");
		elecText.setTextDate(new Date());
		elecText.setTextRemark("我是学生");
		elecTextService.saveElecText(elecText);
		//需要在.cfg.xml中配置事务为自动提交模式，数据才能持久化到数据库中
	}
	@Test
	public void saveElecText2(){
		IElecTextService elecTextService=(IElecTextService)ServiceProvider.getService(IElecTextService.SERVICE_NAME);
		//实例化po对象，赋值，保存
		ElecText elecText = new ElecText();
		elecText.setTextName("测试Service名称3");
		elecText.setTextDate(new Date());
		elecText.setTextRemark("测试Service备注3");
		elecTextService.saveElecText(elecText);
		//需要在.cfg.xml中配置事务为自动提交模式，数据才能持久化到数据库中
	}
	/*
	 * 通过条件查询对象列表集合
	 * 模仿action层
	 */
	@Test
	public void findCollection(){
		IElecTextService elecTextService=(IElecTextService)ServiceProvider.getService(IElecTextService.SERVICE_NAME);
		//实例化vo对象，赋值，保存
		ElecTextForm elecTextForm = new ElecTextForm();
		elecTextForm.setTextName("赵");
		elecTextForm.setTextRemark("医生");
		List<ElecText> list=elecTextService.findCollectionByConditionNopage(elecTextForm);//通过条件查询对象集合，无分页
		//需要在.cfg.xml中配置事务为自动提交模式，数据才能持久化到数据库中
	}
	
	
}
