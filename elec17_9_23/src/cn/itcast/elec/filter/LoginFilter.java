package cn.itcast.elec.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.elec.domain.ElecUser;

/**
 * 添加过滤器
 * 作用：过滤*.do和*.jsp的连接
 * @author Administrator
 *
 */
public class LoginFilter implements Filter {
	/**
	 * 定义系统页面访问中可放行的链接
	 */
	List<String> list = new ArrayList<String>();
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		list.add("/index.jsp");
		list.add("/image.jsp");
		list.add("/system/elecMenuAction_home.do");
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain chain) throws IOException, ServletException {
			HttpServletRequest request = (HttpServletRequest)servletRequest;
			HttpServletResponse response = (HttpServletResponse)servletResponse;
			// 获取页面中访问的路径链接
			String path = request.getServletPath();
			// 如果可放行的路径中包含访问的路径，则放行
			if(list!=null && list.contains(path)){
				chain.doFilter(request, response);
				return;
			}
			// 从session(globle_user)中获取当前登录用户
			ElecUser elecUser = (ElecUser) request.getSession().getAttribute("globle_user");
			// 如果用户不为空，则放行
			if(elecUser!=null){
				chain.doFilter(request, response);
				return;
			}
			// 如果以上两个条件都不满足，则返回系统登录页面
			response.sendRedirect(request.getContextPath()+"/");
	}

	@Override
	public void destroy() {
		
	}

}
