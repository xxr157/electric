package cn.itcast.elec.common;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.itcast.elec.common.ICommonDao;
import cn.itcast.elec.util.GenericSuperClass;
import cn.itcast.elec.util.PageInfo;
/*
 * spring提供的HibernateDaoSupport，里边封装了hibernate操作的增删改查方法
 * 所以需要配置spring的beans.xml文件
 */
public class CommonDaoImpl<T> extends HibernateDaoSupport implements ICommonDao<T>{
	//泛型转换
	private Class entity=(Class)GenericSuperClass.getClass(this.getClass());
	// 保存
	@Override
	public void save(T entity) {
		//获取hibernate模板(需要注入sessionFactory才能获取)
		this.getHibernateTemplate().save(entity);
	}
	/**
	 * 也可以通过在beans.xml中写xml给类注入sessionFactory（不用）
	 *  <bean id="xxxxx" class="cn.itcast.elec.dao.impl.CommonDaoImpl">
			<property name="sessionFactory" ref="sessionFactory"></property>
		</bean>
	 */
	//注入sessionFactory
	@Resource(name="sessionFactory")
	public final void setSessionFactoryDi(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	//修改
	@Override
	public void update(T entity) {
		//获取hibernate模板(需要注入sessionFactory才能获取)
		this.getHibernateTemplate().update(entity);
		// 清理缓存，强制数据库与Hibernate缓存同步，确保在事务提交之前执行update方法
		this.getHibernateTemplate().flush();
	}
	//通过主键查询对象
	@Override
	public T findObjectById(Serializable id) {
		//泛型转换（将下面这两行功能写到工具类GenericSuperClass里)
//		ParameterizedType pt=(ParameterizedType)this.getClass().getGenericSuperclass();
//		Class entity=(Class)pt.getActualTypeArguments()[0];
		//写为成员变量
		//Class entity=(Class)GenericSuperClass.getClass(this.getClass());
		
		return (T)this.getHibernateTemplate().get(entity, id);
	}
	//通过id数组删除对象
	@Override
	public void deleteObjectByIDs(Serializable... ids) {
		for(int i=0;ids!=null && i<ids.length;i++){
			Serializable id=ids[i];
			//通过id查询对象
			Object object=(Object)this.getHibernateTemplate().get(entity, id);
			//删除
			this.getHibernateTemplate().delete(object);
		}
	}
	//通过集合对象删除
	@Override
	public void deleteObjectByCollection(Collection<T> entities) {
		this.getHibernateTemplate().deleteAll(entities);
	}
	//通过条件查询对象集合，无分页
	@Override
	public List<T> findCollectionByConditionNopage(String hqlWhere,
			final Object[] params, LinkedHashMap<String, String> orderby) {
		/**
		 * 组织HQL语句的Where条件
		 *      select * from elec_text o where 1=1     放置DAO层
				and o.textName like ?              放置Service层
				and o.textRemark like ?
				order by o.textDate desc , o.textName asc 
		 */
		//entity.getSimpleName()获取实体的类
		String hql = "from " + entity.getSimpleName() + " o where 1=1";
		//组织排序条件(将其功能实现封装起来，见下面orderByCondition方法）
		String hqlOrderBy = this.orderByCondition(orderby);
		hql = hql + hqlWhere + hqlOrderBy;
		final String finalHql = hql;
		List<T> list = (List<T>)this.getHibernateTemplate().execute(new HibernateCallback(){
            public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(finalHql);
				setParams(query,params);
				return query.list();
			}
		});
		return list;
	}

	/**  
	* @Name: setParams
	* @Description: 对where条件中的参数设置参数值
	* @Parameters: Object[] params 参数值
	* @Return: 无
	*/
	private void setParams(Query query,Object[] params) {
		for(int i=0;params!=null && i<params.length;i++){
			query.setParameter(i, params[i]);
		}
	}

	/**  
	* @Description: 组织排序条件
	* @Parameters: LinkedHashMap<String, String> orderby 排序条件
	* @Return: String 排序语句的字符串
	*/
	private String orderByCondition(LinkedHashMap<String, String> orderby) {
		StringBuffer buffer = new StringBuffer("");
		if(orderby!=null){
			buffer.append(" order by ");
			for(Map.Entry<String, String> map:orderby.entrySet()){
				buffer.append(" " + map.getKey() + " " + map.getValue() + ",");
			}
			buffer.deleteCharAt(buffer.length()-1);//去掉最后一个逗号
		}
		return buffer.toString();
	}
	/**  
	* @Description: 使用集合的形式进行批量保存
	* @Parameters: List<T> entities实体集合
	* @create date 2017-12-27
	* @Return: 无
	*/
	@Override
	public void saveObjectByCollection(Collection<T> entities) {
		this.getHibernateTemplate().saveOrUpdateAll(entities);
	}
	/**  
	* @Name: findCollectionByConditionWithPage
	* @Description: 使用查询条件查询列表的集合（分页）
	* @Create Date: 2018-02-27 （创建日期）
	* @Parameters: String hqlWhere hql语句的where条件
	*              Object[] params where条件的查询参数
	*              LinkedHashMap<String, String> orderby 排序条件
	* @Return: List<T> 结果集列表集合
	*/
	public List<T> findCollectionByConditionWithPage(String hqlWhere,
			final Object[] params, LinkedHashMap<String, String> orderby,
			final PageInfo pageInfo) {
		String hql = "from " + entity.getSimpleName() + " o where 1=1";
		//组织排序条件
		String hqlOrderBy = this.orderByCondition(orderby);
		hql = hql + hqlWhere + hqlOrderBy;
		final String finalHql = hql;
		List<T> list = (List<T>)this.getHibernateTemplate().execute(new HibernateCallback(){
            public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(finalHql);
				setParams(query,params);
				//添加分页功能
				pageInfo.setTotalResult(query.list().size()); // 通过pageInfo对象设置列表中的总记录数
				query.setFirstResult(pageInfo.getBeginResult());  // 当前页中的数据从第几条开始查询
				query.setMaxResults(pageInfo.getPageSize());   // 当前页显示几条记录
				return query.list();
			}
		});
		return list;
	}
}
