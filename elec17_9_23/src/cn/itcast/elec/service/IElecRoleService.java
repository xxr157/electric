package cn.itcast.elec.service;

import java.util.List;

import cn.itcast.elec.util.XmlObject;
import cn.itcast.elec.web.form.ElecRoleForm;
import cn.itcast.elec.web.form.ElecUserForm;


public interface IElecRoleService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecRoleServiceImpl";
	
	// 从Function.xml配置文件中获取权限集合
	List<XmlObject> readXmlObject();

	List<XmlObject> readEditXml(String roleID);

	List<ElecUserForm> findElecUserListByRoleID(String roleID);

	void saveRole(ElecRoleForm elecRoleForm);

}
