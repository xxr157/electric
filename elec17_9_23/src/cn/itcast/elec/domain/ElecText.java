package cn.itcast.elec.domain;

import java.util.Date;

/**
 * po持久层对象，对应数据库表Elec_Text
 * @author Administrator
 * 
 * 问题:当重启服务器后刷新已登录用户页面会退出登录，已不在是登录状态
 * 解决问题：服务器关闭后会在apache-tomcat-7.0.42\work\Catalina\localhost\OA17_8_15目录下生成一个SESSIONS.ser文件以保存session信息，
 *   虽然user是保存在session中的，但我们都User类不能被序列化，服务器关闭不会将用户登录信息写入SESSIONS.ser文件中
 *   所以需要实现序列化接口java.io.Serializable，才能被写入文件中
 *
 */

@SuppressWarnings("serial")
public class ElecText implements java.io.Serializable {
	private String textID;
	private String textName;
	private Date textDate;
	private String textRemark;//备注
	
	public String getTextID() {
		return textID;
	}
	public void setTextID(String textID) {
		this.textID = textID;
	}
	public String getTextName() {
		return textName;
	}
	public void setTextName(String textName) {
		this.textName = textName;
	}
	public Date getTextDate() {
		return textDate;
	}
	public void setTextDate(Date textDate) {
		this.textDate = textDate;
	}
	public String getTextRemark() {
		return textRemark;
	}
	public void setTextRemark(String textRemark) {
		this.textRemark = textRemark;
	}
	@Override
	public String toString() {
		return "ElecText [textID=" + textID + ", textName=" + textName
				+ ", textDate=" + textDate + ", textRemark=" + textRemark + "]";
	}
	
}
