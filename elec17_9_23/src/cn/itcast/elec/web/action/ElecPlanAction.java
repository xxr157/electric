package cn.itcast.elec.web.action;


import cn.itcast.elec.web.form.ElecEquapmentForm;
import cn.itcast.elec.web.form.ElecPlanForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * 设备购置计划
 * @author Administrator
 *
 */
public class ElecPlanAction extends BaseAction implements ModelDriven<ElecPlanForm>{
	private ElecPlanForm elecPlanForm=new ElecPlanForm();
	// 写在父类中了
	//private HttpServletRequest request=null;
	// 控制层调用业务层
//	private IElecUserService elecUserService=(IElecUserService)ServiceProvider.getService(IElecUserService.SERVICE_NAME);
//	// 引入数据字典
//	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);

	@Override
	public ElecPlanForm getModel() {
		return elecPlanForm;
	}
	/**
	 * @description 查询所有用户信息
	 * @create date 2017-12-29
	 * @param 无
	 * @return String home跳转到userIndex.jsp
	 */
	public String home(){
//		List<ElecUserForm> list = elecUserService.findElecUserList(elecUserForm);
//		request.setAttribute("userList", list);
		return "home";
	}
	/**
	 * @description 添加用户
	 * @create date 2017-12-30
	 * @param 无
	 * @return String add跳转到userAdd.jsp
	 */
	
}
