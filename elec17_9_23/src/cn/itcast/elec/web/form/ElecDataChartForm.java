package cn.itcast.elec.web.form;

import java.util.Date;

/**
 * vo值对象，对应页面表单的属性值
 * vo对象与po对象的关系：
 * 	        相同点：都是javabean
 *     不同点：PO对象中的属性关联数据库的字段
 *            VO对象中的属性可以随意增加、修改、删除，对应的页面表单属性
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecDataChartForm implements java.io.Serializable{
	private String userID;        //主键ID
	private String jctID;         //所属单位code
	private String userName;      //用户姓名
	private String logonName;     //登录名
	private String logonPwd;      //密码
	private String sexID;         //性别
	private String birthday;        //出生日期
	private String address;       //联系地址
	private String contactTel;    //联系电话 
	private String email;         //电子邮箱
	private String mobile;        //手机
	private String isDuty;        //是否在职
	private String onDutyDate;      //入职时间
	private String offDutyDate;     //离职时间
	private String remark;        //备注
	/*
	 * 使用viewflag字段判断当前用户操作的是编辑还是查询明细
	 *  如果viewflag==null，则说明用户操作的是编辑操作
	 *  如果viewflag==1，则说明用户操作的是查询明细操作
	 */
	private String viewflag;
	/*
	 * 使用flag字段
	 * 判断角色编辑的页面中，该用户是否被选中
	 *  * 如果 flag = 0，表示该角色不拥有此用户，则页面中用户复选框不被选中
     *  * 如果 flag = 1，表示该角色拥有此用户，则页面中的用户复选框被选中
	 */
	private String flag;

	/*
	 * 用于判断处理当前用户是否修改了密码，利用md5flag进行标识和判断
         * 如果当前用户修改了密码，则保存运行时，需要对密码进行加密，然后保存加密后的密码
            * 设置md5flag == null
         * 如果当前用户没有修改密码，则保存运行时，不需要对密码进行加密，直接保存当前密码即可
            * 设置md5falg == 1
	 */
	private String md5flag;
	/*
	 * 如果roleflag=1，则按保存按钮时跳转到编辑页面，否则跳转到用户列表userIndex.jsp页面
	 * 在left.jsp和ElecUserAction中判断
	 */
	private String roleflag;
	
	public String getRoleflag() {
		return roleflag;
	}
	public void setRoleflag(String roleflag) {
		this.roleflag = roleflag;
	}
	public String getMd5flag() {
		return md5flag;
	}
	public void setMd5flag(String md5flag) {
		this.md5flag = md5flag;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getViewflag() {
		return viewflag;
	}
	public void setViewflag(String viewflag) {
		this.viewflag = viewflag;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getJctID() {
		return jctID;
	}
	public void setJctID(String jctID) {
		this.jctID = jctID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLogonName() {
		return logonName;
	}
	public void setLogonName(String logonName) {
		this.logonName = logonName;
	}
	public String getLogonPwd() {
		return logonPwd;
	}
	public void setLogonPwd(String logonPwd) {
		this.logonPwd = logonPwd;
	}
	public String getSexID() {
		return sexID;
	}
	public void setSexID(String sexID) {
		this.sexID = sexID;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactTel() {
		return contactTel;
	}
	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getIsDuty() {
		return isDuty;
	}
	public void setIsDuty(String isDuty) {
		this.isDuty = isDuty;
	}
	public String getOnDutyDate() {
		return onDutyDate;
	}
	public void setOnDutyDate(String onDutyDate) {
		this.onDutyDate = onDutyDate;
	}
	public String getOffDutyDate() {
		return offDutyDate;
	}
	public void setOffDutyDate(String offDutyDate) {
		this.offDutyDate = offDutyDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
