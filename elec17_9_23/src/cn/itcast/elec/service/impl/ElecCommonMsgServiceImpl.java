package cn.itcast.elec.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecCommonMsgDao;
import cn.itcast.elec.domain.ElecCommonMsg;
import cn.itcast.elec.service.IElecCommonMsgService;
import cn.itcast.elec.web.form.ElecCommonMsgForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecCommonMsgService.SERVICE_NAME)
public class ElecCommonMsgServiceImpl implements IElecCommonMsgService{
	@Resource(name=IElecCommonMsgDao.SERVICE_NAME)
	private IElecCommonMsgDao elecCommonMsgDao;
	/*
	 * 查询所有待办事宜信息
	 */
	@Override
	public List<ElecCommonMsgForm> findElecCommonMsgList() {
		String hqlWhere = "";
		Object[] params = null;
		LinkedHashMap<String,String> orderby = new LinkedHashMap<String,String>();
		orderby.put("o.createDate", "desc"); // 按日期降序排序
		// 查询得到po对象
		List<ElecCommonMsg> list = elecCommonMsgDao.findCollectionByConditionNopage(hqlWhere, params, orderby); 
		// 将po对象转换为vo对象
		List<ElecCommonMsgForm> formList = this.elecCommonMsgPOListToVOList(list);
		return formList;
	}
	/*
	 * 将查询到的待办事宜信息从PO对象转换为VO对象
	 */
	private List<ElecCommonMsgForm> elecCommonMsgPOListToVOList(
			List<ElecCommonMsg> list) {
		List<ElecCommonMsgForm> formList = new ArrayList<ElecCommonMsgForm>();
		ElecCommonMsgForm elecCommonMsgForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			ElecCommonMsg elecCommonMsg =list.get(i);
			elecCommonMsgForm = new ElecCommonMsgForm();
			elecCommonMsgForm.setComID(elecCommonMsg.getComID());
			elecCommonMsgForm.setStationRun(elecCommonMsg.getStationRun());
			elecCommonMsgForm.setDevRun(elecCommonMsg.getDevRun());
			// 将Date类型转换为String类型
			elecCommonMsgForm.setCreateDate(String.valueOf(elecCommonMsg.getCreateDate()!=null?elecCommonMsg.getCreateDate():""));
			formList.add(elecCommonMsgForm);
		}
		return formList;
	}
	/*
	 * 将页面中新建的待办事宜信息保存到数据库中
	 * 页面数据持久化到数据库中
	 * 需要将保存方法定义为可写
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	@Override
	public void saveElecCommonMsg(ElecCommonMsgForm elecCommonMsgForm) {
		// VO对象转换成PO对象
		ElecCommonMsg elecCommmonMsg = this.elecCommonMsgVOToPO(elecCommonMsgForm);
		// 保存到数据库中
		elecCommonMsgDao.save(elecCommmonMsg);
	}
	/*
	 * 将页面待办事宜信息从VO对象转换成PO对象
	 */
	private ElecCommonMsg elecCommonMsgVOToPO(ElecCommonMsgForm elecCommonMsgForm) {
		ElecCommonMsg elecCommonMsg = new ElecCommonMsg();
		if(elecCommonMsgForm!=null){
			elecCommonMsg.setStationRun(elecCommonMsgForm.getStationRun());
			elecCommonMsg.setDevRun(elecCommonMsgForm.getDevRun());
			elecCommonMsg.setCreateDate(new Date());
		}
		return elecCommonMsg;
	}
	/**
	 * @description 通过当前日期查询待办事宜列表
	 * @create date 2017-12-24
	 * @return List<ElecCommonMsgForm>待办事宜列表
	 */
	@Override
	public List<ElecCommonMsgForm> findElecCommonMsgByCurruntDate() {
		// 获取当期日期
		java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
		String currentDate = date.toString();
		// 组织查询条件
//		String hqlWhere = " and o.createDate=?";
//		Object[] params = {date};
//		LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
//		orderby.put("createDate", "desc");
//		List<ElecCommonMsg> list = elecCommonMsgDao.findCollectionByConditionNopage(hqlWhere, params, orderby);
//		// 将po对象转换为vo对象
//		List<ElecCommonMsgForm> formList = this.elecCommonMsgPOListToVOList(list);
		
		List<Object[]> list = elecCommonMsgDao.findElecCommonMsgByCurruntDate(currentDate);
		// 将Object数组转换为VO对象
		List<ElecCommonMsgForm> formList = this.elecCommonMsgObjectListToVOList(list);
		return formList;
	}
	// 将Object数组转换为VO对象
	private List<ElecCommonMsgForm> elecCommonMsgObjectListToVOList(
			List<Object[]> list) {
		List<ElecCommonMsgForm> formList = new ArrayList<ElecCommonMsgForm>();
		ElecCommonMsgForm elecCommonMsgForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			Object[] object = list.get(i);
			elecCommonMsgForm = new ElecCommonMsgForm();
			elecCommonMsgForm.setStationRun(object[0].toString());
			elecCommonMsgForm.setDevRun(object[1].toString());
			formList.add(elecCommonMsgForm);
		}
		return formList;
	}
	
}
