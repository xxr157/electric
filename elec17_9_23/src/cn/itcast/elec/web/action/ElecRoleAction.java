package cn.itcast.elec.web.action;

import java.util.List;

import cn.itcast.elec.container.ServiceProvider;
import cn.itcast.elec.service.IElecRoleService;
import cn.itcast.elec.service.IElecSystemDDLService;
import cn.itcast.elec.util.XmlObject;
import cn.itcast.elec.web.form.ElecRoleForm;
import cn.itcast.elec.web.form.ElecSystemDDLForm;
import cn.itcast.elec.web.form.ElecUserForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * @author Administrator
 *
 */
public class ElecRoleAction extends BaseAction implements ModelDriven<ElecRoleForm>{
	private ElecRoleForm elecRoleForm=new ElecRoleForm();
	// 写在父类中了
	//private HttpServletRequest request=null;
	// 控制层调用业务层
	private IElecRoleService elecRoleService=(IElecRoleService)ServiceProvider.getService(IElecRoleService.SERVICE_NAME);
	// 引入数据字典
	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);
	@Override
	public ElecRoleForm getModel() {
		return elecRoleForm;
	}
	/**
	 * @description 查询所有角色类型（在数据字典中获取）
	 * 				从Function.xml文件中查询系统中所有的功能权限
	 * @create date 2017-1-5
	 * @param 无
	 * @return String home跳转到roleIndex.jsp
	 */
	public String home(){
		// 通过页面选中的keyword获取所有数据类型
		List<ElecSystemDDLForm> systemList = elecSystemDDLService.findElecSystemDDLListByKeyword("角色类型");
		request.setAttribute("systemList", systemList);
		// 从Function.xml配置文件中获取权限集合
		List<XmlObject> xmlList = elecRoleService.readXmlObject();
		request.setAttribute("xmlList", xmlList);
		return "home";
	}
	/**
	 * @description 1.使用角色id查询该角色下具有的权限，并与系统中所有的权限进行匹配
	 * 				2.使用角色ID查询该角色所拥有的用户
	 * @create date 2018-01-22
	 * @param 无
	 * @return String home跳转到roleEdit.jsp
	 */
	public String edit(){
		// 获取页面传递过来的角色id
		String roleID = elecRoleForm.getRole();
		List<XmlObject> xmlList = elecRoleService.readEditXml(roleID);
		request.setAttribute("xmlList", xmlList);
		// 查询用户集合
		List<ElecUserForm> userList = elecRoleService.findElecUserListByRoleID(roleID);
		request.setAttribute("userList", userList);
		return "edit";
	}
	/**
	 * @description 执行保存:保存角色和权限的关联表
	 * 					      保存用户和角色的关联表
	 * @create date 2018-01-22
	 * @param 无
	 * @return String save重定向到roleIndex.jsp
	 */
	public String save(){
		elecRoleService.saveRole(elecRoleForm);
		return "save";
	}
}
