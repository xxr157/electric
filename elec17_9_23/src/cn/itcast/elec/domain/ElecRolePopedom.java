package cn.itcast.elec.domain;

import java.util.Date;

/**
 * 角色权限
 * po持久层对象，对应数据库表Elec_Role_Popedom
 * @author Administrator
 * 
 * 问题:当重启服务器后刷新已登录用户页面会退出登录，已不在是登录状态
 * 解决问题：服务器关闭后会在apache-tomcat-7.0.42\work\Catalina\localhost\OA17_8_15目录下生成一个SESSIONS.ser文件以保存session信息，
 *   虽然user是保存在session中的，但我们都User类不能被序列化，服务器关闭不会将用户登录信息写入SESSIONS.ser文件中
 *   所以需要实现序列化接口java.io.Serializable，才能被写入文件中
 *
 */

@SuppressWarnings("serial")
public class ElecRolePopedom implements java.io.Serializable {
	private String roleID;      // 主键ID
	private String popedomCode; // 配置web文件中权限的编码code的字符窜连接
	private String remark; // 备注
	
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getPopedomCode() {
		return popedomCode;
	}
	public void setPopedomCode(String popedomCode) {
		this.popedomCode = popedomCode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
