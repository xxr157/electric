package cn.itcast.elec.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecLogDao;
import cn.itcast.elec.domain.ElecLog;
import cn.itcast.elec.domain.ElecUser;
import cn.itcast.elec.service.IElecLogService;
import cn.itcast.elec.web.form.ElecLogForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecLogService.SERVICE_NAME)
public class ElecLogServiceImpl implements IElecLogService{
	@Resource(name=IElecLogDao.SERVICE_NAME)
	private IElecLogDao elecLogDao;
	
	/**
	 * @description 保存日志信息
	 * @param request存放用户相关信息 details操作明细
	 * @return 无
	 * @create date 2018-02-25
	 */
	@Override
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	public void saveElecLog(HttpServletRequest request, String details) {
		ElecLog elecLog = new ElecLog();
		elecLog.setIpAddress(request.getRemoteAddr());// ip地址
		ElecUser elecUser = (ElecUser)request.getSession().getAttribute("globle_user");
		elecLog.setOpeName(elecUser.getUserName()); // 操作人
		elecLog.setOpeTime(new Date()); // 操作时间
		elecLog.setDetails(details); // 操作明细
		// 保存
		elecLogDao.save(elecLog);
	}
	
	/**
	 * @description 查询日志列表信息
	 * @param ElecLogForm elecLogForm 用于存放操作人信息
	 * @return List<ElecLogForm>存储日志列表信息
	 * @create date 2018-02-25
	 */
	@Override
	public List<ElecLogForm> findElecLogListByCondition(ElecLogForm elecLogForm) {
		// 组织查询和排序条件
		String hqlWhere = "";
		List<String> paramsList = new ArrayList<String>();
		if(elecLogForm!=null && elecLogForm.getOpeName()!=null && !"".equals(elecLogForm.getOpeName())){
			hqlWhere+=" and o.opeName like ?";
			paramsList.add("%"+elecLogForm.getOpeName()+"%");
		}
		// list集合转换成Object数组
		Object[] params = paramsList.toArray();
		LinkedHashMap<String,String> orderby = new LinkedHashMap<String,String>();
		// 按时间降序排序
		orderby.put("o.opeTime", "desc");
		// 查询
		List<ElecLog> list = elecLogDao.findCollectionByConditionNopage(hqlWhere, params, orderby);
		// PO对象转换成VO对象
		List<ElecLogForm> formList = this.elecLogPOListToVOList(list);
		return formList;
	}
	// PO对象转换成VO对象
	private List<ElecLogForm> elecLogPOListToVOList(List<ElecLog> list) {
		List<ElecLogForm> formList = new ArrayList<ElecLogForm>();
		ElecLogForm elecLogForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			ElecLog elecLog = list.get(i);
			elecLogForm = new ElecLogForm();
			elecLogForm.setDetails(elecLog.getDetails());
			elecLogForm.setIpAddress(elecLog.getIpAddress());
			elecLogForm.setLogID(elecLog.getLogID());
			elecLogForm.setOpeName(elecLog.getOpeName());
			elecLogForm.setOpeTime(String.valueOf(elecLog.getOpeTime()!=null?elecLog.getOpeTime():""));
			formList.add(elecLogForm);
		}
		return formList;
	}
	
	/**
	 * @description 删除查询得到的日志列表信息
	 * @param ElecLogForm elecLogForm 用于存放查询得到的logid数组
	 * @return 无
	 * @create date 2018-02-25
	 */
	@Override
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	public void deleteElecLogByLogIds(ElecLogForm elecLogForm) {
		/*
		 *  第一种方式：在ElecLogForm中定义logid数组，
		 *  直接从页面获取logid数组，通过数组删除列表
		 *  第三种方式：直接在ElecLogAction中使用过request获取logID数组
		 */
		String[] logids = elecLogForm.getLogid();
		elecLogDao.deleteObjectByIDs(logids);
		/*
		 * 第二种方式：通过单个logID获取页面所有的logID值，
		 * 获取的logID以逗号相连,将其按逗号分割成数组的形式在删除
		 */
//		String logID = elecLogForm.getLogID();
//		String[] logids = logID.split(",");
//		// 去掉空格
//		String [] ids = new String[logids.length];
//		for(int i=0;logids!=null && i<logids.length;i++){
//			String logid = logids[i];
//			ids[i] = logid.trim();
//		}
//		elecLogDao.deleteObjectByIDs(ids);
	}
	
}
