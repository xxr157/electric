package cn.itcast.elec.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.itcast.elec.common.CommonDaoImpl;
import cn.itcast.elec.dao.IElecSystemDDLDao;
import cn.itcast.elec.domain.ElecSystemDDL;
//用spring注入dao实现对象
@Repository(IElecSystemDDLDao.SERVICE_NAME)
public class ElecSystemDDLDaoImpl extends CommonDaoImpl<ElecSystemDDL> implements IElecSystemDDLDao {
	/**
	 * @description 查询数据字典中所有数据类型，去掉重复值（distinct）
	 * @create date 2017-12-26
	 * @return List<Object>数据类型列表
	 */
	@Override
	public List<Object> findKeyWord() {
		String hql = "SELECT DISTINCT o.keyword FROM ElecSystemDDL o";
		// 数组
		List<Object> list = this.getHibernateTemplate().find(hql);
		return list;
	}
	/**
	 * @description 通过数据项编号code和对应数据类型查询数据项名称value值
	 * @create date 2017-12-29
	 * @param String ddlCode数据项编号 String keyword所属数据类型
	 * @return String数据项名称
	 */
	@Override
	public String findDDLName(String ddlCode, String keyword) {
		String hql = "from ElecSystemDDL where keyword='" + keyword +"'"+ 
				" and ddlCode=" + ddlCode;
		List<ElecSystemDDL> list = this.getHibernateTemplate().find(hql);
		String ddlName ="";
		if(list!=null && list.size()>0){
			ElecSystemDDL elecSystemDDL = list.get(0);
			ddlName = elecSystemDDL.getDdlName();
		}
		return ddlName;
	}
	
}
