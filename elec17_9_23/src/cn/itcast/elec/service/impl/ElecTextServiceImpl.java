package cn.itcast.elec.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecTextDao;
import cn.itcast.elec.domain.ElecText;
import cn.itcast.elec.service.IElecTextService;
import cn.itcast.elec.web.form.ElecTextForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecTextService.SERVICE_NAME)
public class ElecTextServiceImpl implements IElecTextService{
	@Resource(name=IElecTextDao.SERVICE_NAME)
	private IElecTextDao elecTextDao;
	/*
	 * 一。Spring在Propagation中规定了7种类型的事务传播行为： 

		REQUIRED：支持当前事务，如果当前没有事务，就新建一个事务。这是最常见的选择。 
		
		SUPPORTS：支持当前事务，如果当前没有事务，就以非事务方式执行。 
		
		MANDATORY：支持当前事务，如果当前没有事务，就抛出异常。 
		
		REQUIRES_NEW：新建事务，如果当前存在事务，把当前事务挂起。 
		
		NOT_SUPPORTED：以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。 
		
		NEVER：以非事务方式执行，如果当前存在事务，则抛出异常。 
		
		NESTED：支持当前事务，如果当前事务存在，则执行一个嵌套事务，如果当前没有事务，就新建一个事务。 (non-Javadoc)
			二、Spring事务的隔离级别
		 1. ISOLATION_DEFAULT： 这是一个PlatfromTransactionManager默认的隔离级别，使用数据库默认的事务隔离级别。另外四个与JDBC的隔离级别相对应
		 2. ISOLATION_READ_UNCOMMITTED： 这是事务最低的隔离级别，它充许令外一个事务可以看到这个事务未提交的数据。这种隔离级别会产生脏读，不可重复读和幻像读。
		 3. ISOLATION_READ_COMMITTED： 保证一个事务修改的数据提交后才能被另外一个事务读取。另外一个事务不能读取该事务未提交的数据
		 4. ISOLATION_REPEATABLE_READ： 这种事务隔离级别可以防止脏读，不可重复读。但是可能出现幻像读。它除了保证一个事务不能读取另一个事务未提交的数据外，还保证了避免下面的情况产生(不可重复读)。
		 5. ISOLATION_SERIALIZABLE 这是花费最高代价但是最可靠的事务隔离级别。事务被处理为顺序执行。除了防止脏读，不可重复读外，还避免了幻像读。
	 */
	//方法级别的事务定义为可写,默认隔离级别isolation，广播通知形式propagation
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	@Override
	public void saveElecText(ElecText elecText) {
		elecTextDao.save(elecText);
	}
	
	/**
	 * 通过条件查询对象集合，无分页
	 */
	@Override
	public List<ElecText> findCollectionByConditionNopage(
			ElecTextForm elecTextForm) {
		/*
		 * 组织HQL语句的Where条件
		 *      select * from elec_text o where 1=1     放置DAO层
				and o.textName like '%张%'              放置Service层
				and o.textRemark like '%学生%'
				order by o.datetime desc , o.textName asc 
		 */
		String hqlWhere="";
		List<String> paramList=new ArrayList<String>();//参数集合
		//如果elecTextForm和其名称不为空
		if(elecTextForm!=null && StringUtils.isNotBlank(elecTextForm.getTextName())){
			hqlWhere+=" and o.textName like ?";
			paramList.add("%"+elecTextForm.getTextName()+"%");
		}
		if(elecTextForm!=null && StringUtils.isNotBlank(elecTextForm.getTextRemark())){
			hqlWhere+=" and o.textRemark like ?";
			paramList.add("%"+elecTextForm.getTextRemark()+"%");
		}
		//集合转为数组
		Object[] params=paramList.toArray();
		/**
		 * 组织排序语句
		 *     order by o.textDate desc , o.textName asc 
		 */
		LinkedHashMap<String, String> orderby=new LinkedHashMap<String, String>();
		orderby.put("o.textDate", "desc");
		orderby.put("o.textName", "asc");
		//查询列表
		List<ElecText> list=elecTextDao.findCollectionByConditionNopage(hqlWhere,params,orderby);
		for(int i=0;list!=null && i<list.size();i++){
			ElecText elecText=list.get(i);
			System.out.println(elecText.getTextName() + "　" + elecText.getTextRemark());
		}
		return null;
	}
	
}
