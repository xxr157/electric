package cn.itcast.elec.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.elec.dao.IElecSystemDDLDao;
import cn.itcast.elec.domain.ElecCommonMsg;
import cn.itcast.elec.domain.ElecSystemDDL;
import cn.itcast.elec.service.IElecSystemDDLService;
import cn.itcast.elec.web.form.ElecCommonMsgForm;
import cn.itcast.elec.web.form.ElecSystemDDLForm;
//严格控制业务层事务：类级别的事务定义为只读模式（只有增删改方法需要事务写的权限，可以在这些方法上赋予事务写的权限
@Transactional(readOnly=true)
@Service(IElecSystemDDLService.SERVICE_NAME)
public class ElecSystemDDLServiceImpl implements IElecSystemDDLService{
	@Resource(name=IElecSystemDDLDao.SERVICE_NAME)
	private IElecSystemDDLDao elecSystemDDLDao;
	/**
	 * @description 查询数据字典中所有数据类型，去掉重复值（distinct）
	 * @create date 2017-12-26
	 * @return List<ElecSystemDDLForm>数据类型列表
	 */
	@Override
	public List<ElecSystemDDLForm> findKeyWord() {
		List<Object> list = elecSystemDDLDao.findKeyWord();
		// 将Object对象转换为VO对象
		List<ElecSystemDDLForm> formList = this.elecSystemDDLObjectToVO(list);
		return formList;
	}
	// 将Object对象转换为VO对象
	private List<ElecSystemDDLForm> elecSystemDDLObjectToVO(List<Object> list) {
		List<ElecSystemDDLForm> formList = new ArrayList<ElecSystemDDLForm>();
		ElecSystemDDLForm elecSystemDDLForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			Object object = list.get(i);
			elecSystemDDLForm = new ElecSystemDDLForm();
			elecSystemDDLForm.setKeyword(object.toString());
			formList.add(elecSystemDDLForm);
		}
		return formList;
	
	}
	/**
	 * @description 通过页面选中的keyword查询数据字典中对应其他数据项
	 * @create date 2017-12-26
	 * @return List<ElecSystemDDLForm>对应数据项的VO集合
	 */
	@Override
	public List<ElecSystemDDLForm> findElecSystemDDLListByKeyword(String keyword) {
		List<ElecSystemDDL> list = this.findSystemDDLByKeyword(keyword);
		// 从PO对象转换为VO对象
		List<ElecSystemDDLForm> formList = this.elecSystemDDLPOListToVOList(list);
		return formList;
	}
	/**
	 * @description 封装查询方法,根据选中数据类型查询数据项，获取数据项列表集合
	 * @create date 2017-12-27
	 * @param keyword 选中数据类型
	 * @return List<ElecSystemDDL>对应数据项PO集合
	 */
	private List<ElecSystemDDL> findSystemDDLByKeyword(String keyword) {
		/*
		 *  select * from elec_systemddl o where o.keyword="所属单位"
		 *   order by o.ddlCode asc;
		 */
		String hqlWhere = " and o.keyword = ?";
		// 参数
		Object[] params = {keyword}; 
		// 通过ddlCode升序排序
		LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
		orderby.put("o.ddlCode", "asc"); 
		// 查询数据库中数据得到PO对象
		List<ElecSystemDDL> list = elecSystemDDLDao.findCollectionByConditionNopage(hqlWhere, params, orderby);
		return list;
	}
	// 将数据项集合列表从PO对象转换为VO对象
	private List<ElecSystemDDLForm> elecSystemDDLPOListToVOList(
			List<ElecSystemDDL> list) {
		List<ElecSystemDDLForm> formList = new ArrayList<ElecSystemDDLForm>();
		ElecSystemDDLForm elecSystemDDLForm = null;
		for(int i=0;list!=null && i<list.size();i++){
			ElecSystemDDL elecSystemDDL =list.get(i);
			elecSystemDDLForm = new ElecSystemDDLForm();
			// 将Integer类型转换为String类型
			elecSystemDDLForm.setSeqID(String.valueOf(elecSystemDDL.getSeqID()));
			elecSystemDDLForm.setKeyword(elecSystemDDL.getKeyword());
			elecSystemDDLForm.setDdlCode(String.valueOf(elecSystemDDL.getDdlCode()));
			elecSystemDDLForm.setDdlName(elecSystemDDL.getDdlName());
			formList.add(elecSystemDDLForm);
		}
		return formList;
	}
	/**
	 * @description 保存数据字典页面中编辑的内容到数据库中，需将此方法定义为可写
	 * @param ElecSystemDDLForm elecSystemDDLForm存放页面传递的表单值
	 * @create date 2017-12-27
	 * @return 无
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED,readOnly=false)
	@Override
	public void saveElecSystemDDL(ElecSystemDDLForm elecSystemDDLForm) {
		/*
		 * 获取页面传递的表单值
		 */
		// 获取数据类型
		String keyword = elecSystemDDLForm.getKeywordname();
		// 获取标识，判断是新增数据类型还是在原有数据类型上进行编辑
		String typeflag = elecSystemDDLForm.getTypeflag();
		// 获取需要保存的数据项名称
		String[] itemname = elecSystemDDLForm.getItemname();
		// 如果是新增数据类型操作，即typeflag=new 
		if(typeflag!=null && typeflag.equals("new")){
			// 保存数据字典
			this.saveSystemDDLWithParams(keyword,itemname);
		}else{ // 如果是在原有数据类型基础上操作，即typeflag=add
			// 根据选中的数据类型，查询该数据类型对应的数据项
			List<ElecSystemDDL> list = findSystemDDLByKeyword(keyword);
			// 通过查询到的集合对象删除该数据类型对应的数据项
			elecSystemDDLDao.deleteObjectByCollection(list);
			// 保存数据字典
			this.saveSystemDDLWithParams(keyword,itemname);
		}
	}
	/**
	 * @description 封装保存方法，通过传递的参数保存数据字典
	 * @param keyword 数据类型
	 * @param itemname 数据项名称
	 * @create date 2017-12-27
	 */
	private void saveSystemDDLWithParams(String keyword, String[] itemname) {
		List<ElecSystemDDL> list = new ArrayList<ElecSystemDDL>();
		for(int i=0;itemname!=null && i<itemname.length;i++){
			ElecSystemDDL elecSystemDDL = new ElecSystemDDL();
			elecSystemDDL.setKeyword(keyword);
			elecSystemDDL.setDdlName(itemname[i]);
			elecSystemDDL.setDdlCode(new Integer(i+1));
			list.add(elecSystemDDL);
//			elecSystemDDLDao.save(elecSystemDDL);
		}
		// 使用集合的形式进行批量保存
		elecSystemDDLDao.saveObjectByCollection(list);
	}
	
}
