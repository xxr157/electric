package cn.itcast.elec.util;

import java.lang.reflect.ParameterizedType;

/**
 * 泛型转换
 *  common包/CommonDaoImpl类中使用了这个功能
 * @author Administrator
 *
 */
public class GenericSuperClass {

	public static Class getClass(Class tClass) {
		ParameterizedType pt=(ParameterizedType)tClass.getGenericSuperclass();
		Class entity=(Class)pt.getActualTypeArguments()[0];
		return entity;
	}
	
}
