package cn.itcast.elec.dao;


import java.util.List;

import cn.itcast.elec.common.ICommonDao;
import cn.itcast.elec.domain.ElecCommonMsg;

public interface IElecCommonMsgDao extends ICommonDao<ElecCommonMsg>{
	public static final String SERVICE_NAME="cn.itcast.elec.dao.impl.ElecCommonMsgDaoImpl";
	// 通过当前日期查询待办事宜列表
	List<Object[]> findElecCommonMsgByCurruntDate(String currentDate);


}
