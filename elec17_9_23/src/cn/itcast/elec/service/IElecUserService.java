package cn.itcast.elec.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.itcast.elec.domain.ElecUser;
import cn.itcast.elec.web.form.ElecUserForm;


public interface IElecUserService {
	public static final String SERVICE_NAME="cn.itcast.elec.service.impl.ElecUserServiceImpl";

	List<ElecUserForm> findElecUserList(ElecUserForm elecUserForm, HttpServletRequest request);

	void saveElecUser(ElecUserForm elecUserForm);

	ElecUserForm findElecUser(ElecUserForm elecUserForm);

	void deleteElecUser(ElecUserForm elecUserForm);

	String checkLogonName(String logonName);

	ElecUser findElecUserByLogonName(String name);

	String findElecPopedomByLogonName(String name);

	Hashtable<String, String> findElecRoleByLogonName(String name);

	ArrayList getExcelFiledNameList();

	ArrayList getExcelFiledDataList(ElecUserForm elecUserForm);

	void saveElecUserWithExcel(ElecUserForm elecUserForm);

	List<ElecUserForm> findUserByChart();

}
