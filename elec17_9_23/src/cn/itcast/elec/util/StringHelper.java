package cn.itcast.elec.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 实现将String类型的日期转换成Date类型
 * @author Administrator
 *
 */
public class StringHelper {

	public static Date stringCoverDate(String datetime) {
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		Date d=null;
		try {
			d=format.parse(datetime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}
	
}
