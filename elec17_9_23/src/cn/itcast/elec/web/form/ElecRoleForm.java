package cn.itcast.elec.web.form;

import java.util.Date;

/**
 * vo值对象，对应页面表单的属性值
 * vo对象与po对象的关系：
 * 	        相同点：都是javabean
 *     不同点：PO对象中的属性关联数据库的字段
 *            VO对象中的属性可以随意增加、修改、删除，对应的页面表单属性
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecRoleForm implements java.io.Serializable{
	private String role;  // 页面roleIndex.jsp传递的角色ID
	private String roleid; // roleIndex.jsp页面隐藏域传递过来的角色id
	private String [] selectoper; // 权限编号（权限code）
	private String [] selectuser; // roleEdit.jsp传递的用户ID
	
	public String getRoleid() {
		return roleid;
	}

	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}

	public String[] getSelectoper() {
		return selectoper;
	}

	public void setSelectoper(String[] selectoper) {
		this.selectoper = selectoper;
	}

	public String[] getSelectuser() {
		return selectuser;
	}

	public void setSelectuser(String[] selectuser) {
		this.selectuser = selectuser;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}
