package cn.itcast.elec.web.action;


import cn.itcast.elec.web.form.ElecSiteMaintainForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * 仪器设备管理
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ElecSiteMaintainAction extends BaseAction implements ModelDriven<ElecSiteMaintainForm>{
	private ElecSiteMaintainForm elecSiteMaintainForm=new ElecSiteMaintainForm();
	// 写在父类中了
	//private HttpServletRequest request=null;
	// 控制层调用业务层
//	private IElecUserService elecUserService=(IElecUserService)ServiceProvider.getService(IElecUserService.SERVICE_NAME);
//	// 引入数据字典
//	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);

	@Override
	public ElecSiteMaintainForm getModel() {
		return elecSiteMaintainForm;
	}
	/**
	 * @description 查询所有用户信息
	 * @create date 2017-12-29
	 * @param 无
	 * @return String home跳转到userIndex.jsp
	 */
	public String home(){
//		List<ElecUserForm> list = elecUserService.findElecUserList(elecUserForm);
//		request.setAttribute("userList", list);
		return "home";
	}
	/**
	 * @description 维护情况
	 * @create date 2018-04-19
	 * @param 无
	 * @return String add跳转到siteStateIndex.jsp
	 */
	public String state(){
		return "state";
	}
	/**
	 * @description 维护计划添加
	 * @create date 2018-04-19
	 * @param 无
	 * @return String add跳转到siteMaintainAdd.jsp
	 */
	public String mainAdd(){
		return "mainAdd";
	}
	/**
	 * @description 维护情况添加
	 * @create date 2018-04-19
	 * @param 无
	 * @return String stateAdd跳转到siteStateAdd.jsp
	 */
	public String stateAdd(){
		return "stateAdd";
	}
	
	 
}
