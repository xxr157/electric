package cn.itcast.elec.test;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import cn.itcast.elec.domain.ElecText;


public class TestHibernate {
	@Test
	public void testElecText(){
		/*
		 * hibernate工作原理
		 */
		//1.读取并解析配置文件hibernate.cfg.xml
		Configuration config=new Configuration();
		config.configure();
		//2.读取并解析映射文件.hbm.xml，创建sessionFactory
		SessionFactory sessionFactory=config.buildSessionFactory();
		//3.打开Session
		Session session=sessionFactory.openSession();
		//4.创建事务Transaction
		Transaction tr=session.beginTransaction();
		//5.持久化操作
		ElecText elecText = new ElecText();
		elecText.setTextName("测试Hibernate名称");
		elecText.setTextDate(new Date());
		elecText.setTextRemark("测试Hibernate备注");
		//保存
		session.save(elecText);
		//6.提交事务
		tr.commit();
		//7.关闭session
		session.close();
		// 不需要关闭
//		sessionFactory.close();

	}
}
