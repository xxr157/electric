package cn.itcast.elec.dao.impl;

import org.springframework.stereotype.Repository;

import cn.itcast.elec.common.CommonDaoImpl;
import cn.itcast.elec.dao.IElecTextDao;
import cn.itcast.elec.domain.ElecText;
//用spring注入dao实现对象
@Repository(IElecTextDao.SERVICE_NAME)
public class ElecTextDaoImpl extends CommonDaoImpl<ElecText> implements IElecTextDao {
	
}
