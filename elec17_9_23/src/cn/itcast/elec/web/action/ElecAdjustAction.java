package cn.itcast.elec.web.action;


import cn.itcast.elec.web.form.ElecAdjustForm;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 控制层
 * 设备校准检修
 * @author Administrator
 *
 */
public class ElecAdjustAction extends BaseAction implements ModelDriven<ElecAdjustForm>{
	private ElecAdjustForm elecAdjustForm=new ElecAdjustForm();
	// 写在父类中了
	//private HttpServletRequest request=null;
	// 控制层调用业务层
//	private IElecUserService elecUserService=(IElecUserService)ServiceProvider.getService(IElecUserService.SERVICE_NAME);
//	// 引入数据字典
//	private IElecSystemDDLService elecSystemDDLService=(IElecSystemDDLService)ServiceProvider.getService(IElecSystemDDLService.SERVICE_NAME);

	@Override
	public ElecAdjustForm getModel() {
		return elecAdjustForm;
	}
	/**
	 * @description 查询所有用户信息
	 * @create date 2017-12-29
	 * @param 无
	 * @return String home跳转到adjustIndex.jsp
	 */
	public String home(){
		return "home";
	}
	/**
	 * @description 校准管理
	 * @create date 2018-04-19
	 * @param 无
	 * @return String adjust跳转到adjustIndex.jsp
	 */
	public String adjust(){
		return "adjust";
	}
	/**
	 * @description 检修管理
	 * @create date 2018-04-19
	 * @param 无
	 * @return String repair跳转到repairIndex.jsp
	 */
	public String repair(){
		return "repair";
	}
	
	
}
