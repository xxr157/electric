package cn.itcast.elec.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import cn.itcast.elec.common.CommonDaoImpl;
import cn.itcast.elec.dao.IElecCommonMsgDao;
import cn.itcast.elec.domain.ElecCommonMsg;
//用spring注入dao实现对象
@Repository(IElecCommonMsgDao.SERVICE_NAME)
public class ElecCommonMsgDaoImpl extends CommonDaoImpl<ElecCommonMsg> implements IElecCommonMsgDao {
	/**
	 * @description 通过当前日期查询待办事宜列表
	 * @create date 2017-12-24
	 * @param String currentDate当前日期
	 * @return List<Object[]>待办事宜列表
	 */
	@Override
	public List<Object[]> findElecCommonMsgByCurruntDate(String currentDate) {
		final String sql = "SELECT o.stationRun as stationRun,o.devRun as devRun "+
					"FROM Elec_CommonMsg o "+
					"WHERE o.createDate = '"+currentDate+"'";
		// 调用hibernate底层方法
		List<Object[]> list = (List<Object[]>)this.getHibernateTemplate().execute(new HibernateCallback(){
			
			@Override
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {
				Query query = session.createSQLQuery(sql)
						.addScalar("stationRun",Hibernate.STRING)
			            .addScalar("devRun",Hibernate.STRING);
				return query.list();
			}
		});
		return list;
	}
	
}
