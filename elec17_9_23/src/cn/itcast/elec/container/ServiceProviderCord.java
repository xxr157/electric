package cn.itcast.elec.container;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 自定义容器
 * 可用于控制层调用业务层
 * @author Administrator
 *
 */
public class ServiceProviderCord {
	protected static ApplicationContext ac;
	
	//加载beans.xml文件（filename放置的是beans.xml）
	public static void load(String filename){
		ac=new ClassPathXmlApplicationContext(filename);
	}
}
